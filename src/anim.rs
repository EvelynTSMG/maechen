#[repr(u8)]
pub enum YojimboReaction {
    /// Internal name: ``
    None = 0x0,
    /// Internal name: `youjinbou_consent_pay`
    Regular = 0x1,
    /// Internal name: `youjinbou_consent_pay_ok`
    Nod = 0x2,
    /// Internal name: `youjinbou_consent_pay_ng`
    Headshake = 0x3,
}

#[repr(u8)]
/// Determines what death animation is applied to the character on death
pub enum DeathAnim {
    /// Internal name: `death_normal`
    ///
    /// Body remains, targetable
    Normal = 0x0, // Characters
    /// Internal name: `death_nop`
    ///
    /// Body remains, untargetable
    Linger = 0x1, // Bosses
    /// Internal name: `death_fadeout`
    ///
    /// Body fades out
    FadeOut = 0x2, // Humanoids
    /// Internal name: `death_phantom`
    ///
    /// Body fades out with Pyreflies
    Pyreflies = 0x3, // Fiends
    /// Internal name: `death_exp`
    ///
    /// Body fades with explosions
    Explosions = 0x4, // Machina death
    /// Internal name: `death_break`
    ///
    /// Body fades out with machina sfx
    BreakDown = 0x5, // Stealing from machina
    /// Internal name: `death_break2`
    ///
    /// Body fades out with bevelle machina sfx?
    BreakDownBevelle = 0x6, // YAT/YKT //TODO: Test what it actually does
}
