#[repr(u32)]
pub enum Sfx {
    StartedNewGame = 0x80000005,
    OpenedChest = 0x80000009,
    PaidGil = 0x8000000E,
    PuzzlePrompt = 0x80000010,
    TakeSphereP1 = 0x80000012,
    InsertSphere = 0x80000013,
    TakeSphereP2 = 0x80000014,
    SolvedPuzzle = 0x80000016,
    GainedLevel = 0x80000017,
    ObtainedTreasure = 0x80000026,
    ObtainedPrimer = 0x80000030,
    SaveSphere = 0x80000032,
    UseCelestialMirror = 0x8000003D,
    BoardAirship = 0x80000048,
    ShowInstructions = 0x8000004A,
    LightningBolt = 0x80015403, //TODO: Check ID of this
}

#[repr(u16)]
pub enum Bgm {
    UnwaveringDetermination = 0x000A,
    SecretManeuvers = 0x000B,
    BossTheme = 0x000C, //? Is this the actual name?
    TheSummoning = 0x000D,
    MacalaniaWoods = 0x000E,
    BattleTheme = 0x0010, //? Unsure
    SeymoursAmbition = 0x001A,
    BlitzOff = 0x001B,
    ThunderPlains = 0x001D,
    PeopleOfTheNorthPole = 0x0025,
    TruthRevealed = 0x0029,
    ToZanarkand = 0x0082,
    Challenge = 0x0091,
}
