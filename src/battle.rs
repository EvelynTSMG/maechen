#[derive(Copy, Clone)]
#[repr(u8)]
/// Determines whether the battle starts with a Pre-Emptive Strike, Ambush, is randomized, or neither
pub enum AmbushState {
    /// Internal name: `first_attack_normal`
    Normal = 0x0,
    /// Internal name: `first_attack_player`
    PreEmptive = 0x1,
    /// Internal name: `first_attack_monster`
    Ambush = 0x2,
    /// Internal name: `first_attack_random_off`
    None = 0x3,
}

impl AmbushState {
    pub fn from(value: u8) -> Option<Self> {
        match value {
            0 => Some(AmbushState::Normal),
            1 => Some(AmbushState::PreEmptive),
            2 => Some(AmbushState::Ambush),
            3 => Some(AmbushState::None),
            _ => None,
        }
    }
}

#[repr(u8)]
/// Determines what battle transition plays when the encounter starts
pub enum BattleTrans {
    Shatter = 0x0,
    Fade = 0x1,
}

#[repr(u8)]
/// Determines type of battle end
pub enum BattleEndType {
    GameOver = 0x1,
    Victory = 0x2,
    Escape = 0x3, //TODO: Test this
}

#[repr(u8)]
/// Determines what formula is used to calculate damage
pub enum DamageFormula {
    None = 0x00,
    /// Strength vs Defense
    StrVsDef = 0x01,
    /// Strength (ignoring Defense)
    StrIgnoreDef = 0x02,
    /// Magic vs Magic Defense
    MagVsMDef = 0x03,
    /// Magic (ignoring Magic Defense)
    MagIgnoreMDef = 0x04,
    /// Multiple (determined by power) of current the target stat / 16,
    ///
    /// e.g. Demi is 4/16 targeting HP (quarter of current HP)
    CurrentDiv16 = 0x05,
    /// Multiple of 50
    Multiple50 = 0x06,
    Healing = 0x07,
    /// Multiple (determined by power) of max target stat / 16
    MaxDiv16 = 0x08,
    /// (Mulitple of 50) ± 12.5%
    Multiple50WithVariance = 0x09,
    /// CurrentDiv16 targetting CTB ticks
    TicksDiv16 = 0x0D,
    /// Special Magic (ignoring magic Defense) (scales like Strength)
    SpecialMag = 0x0F,
    /// (User's Max HP / 10) * power
    FixedUserMaxHp = 0x10,
    /// (chosen Gil / 10) * power
    FixedChosenGil = 0x15,
    /// power * target kills (Karma)
    FixedKills = 0x16,
    /// 9999
    Fixed9999 = 0x17,
}

#[repr(u8)]
/// What type of damage is dealt. Used for physical and magical invincibilities?
pub enum DamageType {
    Special = 0x0,
    Physical = 0x1,
    Magical = 0x2,
}

#[repr(u16)]
/// Actor property IDs used by scripts to access actor stats
pub enum ActorProperty {
    /// Internal name: `stat_hp`
    Hp = 0x0000,
    /// Internal name: `stat_mp`
    Mp = 0x0001,
    /// Internal name: `stat_maxhp`
    MaxHp = 0x0002,
    /// Internal name: `stat_maxmp`
    MaxMp = 0x0003,
    /// Internal name: `stat_alive`
    IsAlive = 0x0004,
    /// Internal name: `stat_poison`
    IsPoisoned = 0x0005,
    /// Internal name: `stat_stone`
    IsPetrified = 0x0006,
    /// Internal name: `stat_zombie`
    IsZombie = 0x0007,
    /// Internal name: `stat_weak`
    ///
    /// Is actor HP below 50% of Max HP
    IsLowHp = 0x0008, //TODO: Test this
    /// Internal name: `stat_str`
    Strength = 0x0009,
    /// Internal name: `stat_vit`
    Defense = 0x000A,
    /// Internal name: `stat_mag`
    Magic = 0x000B,
    /// Internal name: `stat_spirit`
    MagicDefense = 0x000C,
    /// Internal name: `stat_dex`
    Agility = 0x000D,
    /// Internal name: `stat_luck`
    Luck = 0x000E,
    /// Internal name: `stat_avoid`
    Evasion = 0x000F,
    /// Internal name: `stat_hit`
    Accuracy = 0x0010,
    /// Internal name: `stat_poison_per`
    PoisonDmg = 0x0011,
    /// Internal name: `stat_limit_type`
    OverdriveMode = 0x0012,
    /// Internal name: `stat_limit_gauge`
    OverdriveCharge = 0x0013,
    /// Internal name: `stat_limit_gauge_max`
    MaxOverdriveCharge = 0x0014,
    /// Internal name: `stat_inbattle`
    IsFrontline = 0x0015,
    /// Internal name: `stat_man`
    IsMale = 0x0016,
    /// Internal name: `stat_woman`
    IsFemale = 0x0017,
    /// Internal name: `stat_summon`
    IsAeon = 0x0018,
    /// Internal name: `stat_monster`
    IsEnemy = 0x0019,
    /// Internal name: `stat_fly`
    IsFlying = 0x001A,
    /// Internal name: `stat_will_die`
    IsDying = 0x001B,
    /// Internal name: `stat_area`
    BattleRow = 0x001C, //TODO: Test this
    /// Internal name: `stat_pos`
    BattleArenaPos = 0x001D, //TODO: Test this
    /// Internal name: `stat_far`
    ///
    /// How far the actor is in the battle field
    BattleDistance = 0x001E,
    /// Internal name: `stat_group`
    Group = 0x001F, //TODO: Figure this out and name it
    /// Internal name: `stat_sp_hard`
    IsArmored = 0x0020,
    /// Internal name: `stat_sp_raiot`
    IsImmuneToFractionalDmg = 0x0021, //TODO: Test this
    /// Internal name: `stat_sp_zombie`
    IsImmuneToLife = 0x0022, //TODO: Test this
    /// Internal name: `stat_sp_see`
    IsImmuneToSensor = 0x0023, //TODO: Test this
    // `IsImmuneToScan` would conflict with 0x00C8
    /// Internal name: `stat_sp_live`
    SpLive = 0x0024, //TODO: Figure out what this is and name it (`stat_sp_live`)
    /// Internal name: `stat_power_break`
    IsPowerBroken = 0x0025,
    /// Internal name: `stat_magic_break`
    IsMagicBroken = 0x0026,
    /// Internal name: `stat_armor_break`
    IsArmorBroken = 0x0027,
    /// Internal name: `stat_mental_break`
    IsMentalBroken = 0x0028,
    /// Internal name: `stat_confuse`
    IsConfused = 0x0029,
    /// Internal name: `stat_berserk`
    IsBerserk = 0x002A,
    /// Internal name: `stat_prov`
    IsProvoked = 0x002B,
    /// Internal name: `stat_threat`
    IsThreatened = 0x002C,
    /// Internal name: `stat_sleep`
    SleepTurnsLeft = 0x002D,
    /// Internal name: `stat_silence`
    SilenceTurnsLeft = 0x002E,
    /// Internal name: `stat_dark`
    DarknessTurnsLeft = 0x002F,
    /// Internal name: `stat_shell`
    ShellTurnsLeft = 0x0030,
    /// Internal name: `stat_protess`
    ProtectTurnsLeft = 0x0031,
    /// Internal name: `stat_reflect`
    ReflectTurnsLeft = 0x0032,
    /// Internal name: `stat_bawater`
    HasNulWater = 0x0033,
    /// Internal name: `stat_bafire`
    HasNulFire = 0x0034,
    /// Internal name: `stat_bathunder`
    HasNulThunder = 0x0035,
    /// Internal name: `stat_bacold`
    HasNulIce = 0x0036,
    /// Internal name: `stat_regen`
    RegenTurnsLeft = 0x0037,
    /// Internal name: `stat_haste`
    HasteTurnsLeft = 0x0038,
    /// Internal name: `stat_slow`
    SlowTurnsLeft = 0x0039,
    /// Internal name: `ability_see`
    HasSensor = 0x003A, //TODO: Test this
    /// Internal name: `ability_lead`
    HasFirstStrike = 0x003B, //TODO: Test this
    /// Internal name: `ability_first`
    HasInitiative = 0x003C, //TODO: Test this
    /// Internal name: `ability_counter`
    HasCounterAttack = 0x003D,
    /// Internal name: `ability_counter2`
    HasEvadeAndCounter = 0x003E, //TODO: Test this
    /// Internal name: `ability_dark`
    HasDarkAttack = 0x003F, //TODO: Figure out what this is
    /// Internal name: `ability_ap2`
    HasDoubleAp = 0x0040, //TODO: Make sure this is double AP
    /// Internal name: `ability_exp2`
    HasDoubleExp = 0x0041, //TODO: Figure out what this is
    /// Internal name: `ability_booster`
    HasMagicBooster = 0x0042, //TODO: Test this
    /// Internal name: `ability_magic_counter`
    HasMagicCounter = 0x0043, //TODO: Test this
    /// Internal name: `ability_medicine`
    HasAlchemy = 0x0044, //TODO: Test this
    /// Internal name: `ability_auto_potion`
    HasAutoPotion = 0x0045, //TODO: Test this
    /// Internal name: `ability_auto_cureall`
    HasAutoMed = 0x0046, //TODO: Test this
    /// Internal name: `ability_auto_phenix`
    HasAutoPhoenix = 0x0047, //TODO: Test this
    /// Internal name: `ability_limitup`
    HasLimitUp = 0x0048, //TODO: Figure out what this is and name it
    /// Internal name: `ability_dream`
    HasDream = 0x0049, //TODO: Figure out what this is and name it
    /// Internal name: `ability_pierce`
    HasPiercing = 0x004A, //TODO: Test this
    /// Internal name: `ability_exchange`
    HasExchange = 0x004B, //TODO: Figure out what this is and name it. Entrust?
    /// Internal name: `ability_hp_recover`
    HasHpRecover = 0x004C, //TODO: Figure out what this is and name it
    /// Internal name: `ability_mp_recover`
    HasMpRecover = 0x004D, //TODO: Figure out what this is and name it
    /// Internal name: `ability_nonencount`
    HasNoEncounter = 0x004E, //TODO: Test this
    /// Internal name: `stat_death_pattern`
    DeathAnim = 0x004F,
    /// Internal name: `stat_event_chr`
    EventChr = 0x0050, //TODO: Figure out what this is and name it
    /// Internal name: `stat_action`
    GetsTurns = 0x0051,
    /// Internal name: `stat_cursor`
    IsTargetable = 0x0052,
    /// Internal name: `stat_ctb_list`
    IsVisibleOnCtb = 0x0053,
    /// Internal name: `stat_visible`
    IsVisible = 0x0054, //TODO: Test this. Conflicts with 0x00AE `stat_hide`
    /// Internal name: `stat_move_area`
    Location1 = 0x0055, //TODO: Figure out what this is and name it better
    /// Internal name: `stat_move_pos`
    Location2 = 0x0056, //TODO: Figure out what this is and name it better
    // Might be a variant of `Eff` considering AnimsVariant is `stat_motionlv`
    /// Internal name: `stat_efflv`
    Efflv = 0x0057, //TODO: Figure out what this is and name it.
    /// Internal name: `stat_model`
    Model = 0x0058, //TODO: Figure out what this is and name it
    /// Internal name: `stat_damage_chr`
    Parent = 0x0059, //TODO: Test this, probably in Seymour >=Natus
    /// Internal name: `stat_move_target`
    LocationTarget = 0x005A, //TODO: Figure out what this is and name it better
    /// Internal name: `stat_motionlv`
    AnimsVariant = 0x005B,
    /// Internal name: `stat_nop`
    IsBoss = 0x005C, //TODO: Test this, similar to DeathAnim.Nop
    /// Internal name: `stat_move_flag`
    MoveFlag = 0x005D, //TODO: Figure out what this is and name it
    /// Internal name: `stat_live_motion`
    LiveMotion = 0x005E, //TODO: Figure out what this is and name it
    /// Internal name: `stat_adjust_pos`
    AdjustPos = 0x005F, //TODO: Figure out what this is and name it
    /// Internal name: `stat_height_on`
    HeightOn = 0x0060, //TODO: Figure out what this is and name it
    /// Internal name: `stat_sleep_recover_flag`
    SleepRecoverFlag = 0x0061, //TODO: Figure out what this is and name it
    /// Internal name: `stat_abs_fire`
    AbsorbsFire = 0x0062,
    /// Internal name: `stat_abs_cold`
    AbsorbsIce = 0x0063,
    /// Internal name: `stat_abs_thunder`
    AbsorbsThunder = 0x0064,
    /// Internal name: `stat_abs_water`
    AbsorbsWater = 0x0065,
    /// Internal name: `stat_abs_holy`
    AbsorbsHoly = 0x0066,
    /// Internal name: `stat_inv_fire`
    IsImmuneToFire = 0x0067,
    /// Internal name: `stat_inv_cold`
    IsImmuneToIce = 0x0068,
    /// Internal name: `stat_inv_thunder`
    IsImmuneToThunder = 0x0069,
    /// Internal name: `stat_inv_water`
    IsImmuneToWater = 0x006A,
    /// Internal name: `stat_inv_holy`
    IsImmuneToHoly = 0x006B,
    /// Internal name: `stat_half_fire`
    ResistsFire = 0x006C,
    /// Internal name: `stat_half_cold`
    ResistsIce = 0x006D,
    /// Internal name: `stat_half_thunder`
    ResistsThunder = 0x006E,
    /// Internal name: `stat_half_water`
    ResistsWater = 0x006F,
    /// Internal name: `stat_half_holy`
    ResistsHoly = 0x0070,
    /// Internal name: `stat_weak_fire`
    IsWeakToFire = 0x0071,
    /// Internal name: `stat_weak_cold`
    IsWeakToIce = 0x0072,
    /// Internal name: `stat_weak_thunder`
    IsWeakToThunder = 0x0073,
    /// Internal name: `stat_weak_water`
    IsWeakToWater = 0x0074,
    /// Internal name: `stat_weak_holy`
    IsWeakToHoly = 0x0075,
    /// Internal name: `stat_adjust_pos_flag`
    AdjustPosFlag = 0x0076, //TODO: Figure out what this is and name it
    /// Internal name: `stat_inv_physic_motion`
    InvPhysicMotion = 0x0077, //TODO: Figure out what this is and name it
    /// Internal name: `stat_inv_magic_motion`
    InvMagicMotion = 0x0078, //TODO: Figure out what this is and name it
    /// Internal name: `stat_steal_count`
    TimesStolenFrom = 0x0079,
    /// Internal name: `stat_wait_motion_flag`
    WaitMotionFlag = 0x007A, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_return_flag`
    AttackReturnFlag = 0x007B, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_normal_frame`
    AttackNormalFrame = 0x007C, //TODO: Figure out what this is and name it
    /// Internal name: `stat_disable_move_flag`
    IsTough = 0x007D, //TODO: Test this
    /// Internal name: `stat_disable_jump_flag`
    IsHeavy = 0x007E, //TODO: Test this
    /// Internal name: `stat_bodyhit_flag`
    BodyHitFlag = 0x007F, //TODO: Figure out what this is and name it
    /// Internal name: `stat_effvar`
    Effvar = 0x0080, //TODO: Figure out what this is and name it, might relate to Efflv
    /// Internal name: `stat_item`
    StealCommonItemType = 0x0081,
    /// Internal name: `stat_item_num`
    StealCommonItemCount = 0x0082,
    /// Internal name: `stat_rareitem`
    StealRareItemType = 0x0083,
    /// Internal name: `stat_rareitem_num`
    StealRareItemCount = 0x0084,
    /// Internal name: `stat_magiclv`
    Magiclv = 0x0085, //TODO: Figure out what this is and name it
    /// Internal name: `stat_appear_motion_flag`
    ShowCreationAnim = 0x0086, //TODO: Test this
    /// Internal name: `stat_cursor_element`
    CursorElement = 0x0087, //TODO: Figure out what this is and name it
    /// Internal name: `stat_limit_bar_flag_cam`
    LimitBarFlagCam = 0x0088, //TODO: Figure out what this is and name it
    /// Internal name: `stat_limit_bar_flag`
    ShowOverdriveBar = 0x0089,
    /// Internal name: `stat_drop1`
    Drop1Chance = 0x008A,
    /// Internal name: `stat_drop2`
    Drop2Chance = 0x008B,
    /// Internal name: `stat_weapon_drop`
    DropGearChance = 0x008C,
    /// Internal name: `stat_steal`
    StealChance = 0x008D,
    /// Internal name: `stat_exist_flag`
    ///
    /// Must be killed for battle to end
    MustBeKilled = 0x008E, //TODO: Test this
    /// Internal name: `stat_live`
    IsScanned = 0x008F, //TODO: Test this
    /// Internal name: `stat_str_memory`
    IsDistilledPower = 0x0090,
    /// Internal name: `stat_mag_memory`
    IsDistilledMana = 0x0091,
    /// Internal name: `stat_dex_memory`
    IsDistilledSpeed = 0x0092,
    /// Internal name: `stat_move_memory`
    IsDistilledUnused = 0x0093,
    /// Internal name: `stat_ability_memory`
    IsDistilledAbility = 0x0094,
    /// Internal name: `stat_dodge`
    ///
    /// Did it use the `Shield` aeon command
    IsShielding = 0x0095,
    /// Internal name: `stat_defend`
    ///
    /// Did it use the `Boost` aeon command
    IsBoosting = 0x0096,
    /// Internal name: `stat_blow`
    IsEjected = 0x0097,
    /// Internal name: `stat_relife`
    HasAutoLife = 0x0098,
    /// Internal name: `stat_curse`
    IsCursed = 0x0099,
    /// Internal name: `stat_defense`
    IsDefending = 0x009A,
    /// Internal name: `stat_protect`
    IsGuarding = 0x009B,
    /// Internal name: `stat_iron`
    IsSentinel = 0x009C,
    /// Internal name: `stat_death_sentence`
    IsDoomed = 0x009D,
    /// Internal name: `stat_motion_type`
    MotionType = 0x009E, //TODO: Figure out what this is and name it
    /// Internal name: `stat_death_sentence_start`
    DoomInitialCounter = 0x009F,
    /// Internal name: `stat_death_sentence_count`
    DoomCurrentCounter = 0x00A0, //TODO: Test this
    /// Internal name: `stat_dmg_dir`
    DmgDir = 0x00A1, //TODO: Figure out what this is and name it
    /// Internal name: `stat_direction_change_flag`
    DirectionChangeFlag = 0x00A2, //TODO: Figure out what this is and name it
    /// Internal name: `stat_direction_change_effect`
    DirectionChangeEffect = 0x00A3, //TODO: Figure out what this is and name it
    /// Internal name: `stat_direction_fix_flag`
    DirectionFixFlag = 0x00A4, //TODO: Figure out what this is and name it
    /// Internal name: `stat_hit_terminate_flag`
    HitTerminateFlag = 0x00A5, //TODO: Figure out what this is and name it
    /// Internal name: `stat_damage_hp`
    DamageTakenHp = 0x00A6,
    /// Internal name: `stat_damage_mp`
    DamageTakenMp = 0x00A7,
    /// Internal name: `stat_damage-ctb`
    DamageTakenCtb = 0x00A8,
    /// Internal name: `stat_appear_invisible_flag`
    AppearInvisibleFlag = 0x00A9, //TODO: Figure out what this is and name it
    /// Internal name: `stat_effect_hit_num`
    EffectHitNum = 0x00AA, //TODO: Figure out what this is and name it
    /// Internal name: `stat_avoid_flag`
    AvoidFlag = 0x00AB, //TODO: Figure out what this is and name it
    /// Internal name: `stat_blow_exist_flag`
    //? Ejected and is alive?
    BlowExistFlag = 0x00AC, //TODO: Figure out what this is and name it
    /// Internal name: `stat_escape_flag`
    EscapeFlag = 0x00AD, //TODO: Figure out what this is and name it
    // Kari has this named ?Visible
    /// Internal name: `stat_hide`
    IsHiding = 0x00AE, //TODO: Figure out what this is and name it (`stat_hide`)
    /// Internal name: `stat_def_death`
    DeathResistance = 0x00AF,
    /// Internal name: `stat_def_zombie`
    ZombieResistance = 0x00B0,
    /// Internal name: `stat_def_stone`
    PetrifyResistance = 0x00B1,
    /// Internal name: `stat_def_poison`
    PoisonResistance = 0x00B2,
    /// Internal name: `stat_def_power_break`
    PowerBreakResistance = 0x00B3,
    /// Internal name: `stat_def_magic_break`
    MagicBreakResistance = 0x00B4,
    /// Internal name: `stat_def_armor_break`
    ArmorBreakResistance = 0x00B5,
    /// Internal name: `stat_def_mental_break`
    MentalBreakResistance = 0x00B6,
    /// Internal name: `stat_def_confuse`
    ConfusionResistance = 0x00B7,
    /// Internal name: `stat_def_berserk`
    BerserkResistance = 0x00B8,
    /// Internal name: `stat_def_prov`
    ProvokeResistance = 0x00B9,
    /// Internal name: `stat_def_threat`
    ///
    /// 0-100%, 255 for immune
    ThreatenChance = 0x00BA,
    /// Internal name: `stat_def_sleep`
    SleepResistance = 0x00BB,
    /// Internal name: `stat_def_silence`
    SilenceResistance = 0x00BC,
    /// Internal name: `stat_def_dark`
    DarknessResistance = 0x00BD,
    /// Internal name: `stat_def_shell`
    ShellResistance = 0x00BE,
    /// Internal name: `stat_def_protess`
    ProtectResistance = 0x00BF,
    /// Internal name: `stat_def_reflect`
    ReflectResistance = 0x00C0,
    /// Internal name: `stat_def_bawater`
    NulWaterResistance = 0x00C1,
    /// Internal name: `stat_def_bafire`
    NulFireResistance = 0x00C2,
    /// Internal name: `stat_def_bathunder`
    NulThunderResistance = 0x00C3,
    /// Internal name: `stat_def_bacold`
    NulIceResistance = 0x00C4,
    /// Internal name: `stat_def_regen`
    RegenResistance = 0x00C5,
    /// Internal name: `stat_def_haste`
    HasteResistance = 0x00C6,
    /// Internal name: `stat_def_slow`
    SlowResistance = 0x00C7,
    /// Internal name: `stat_def_live`
    IsImmuneToScan = 0x00C8, //TODO: Test this
    /// Internal name: `stat_def_str_memory`
    IsImmuneToDistillPower = 0x00C9,
    /// Internal name: `stat_def_mag_memory`
    IsImmuneToDistillMana = 0x00CA,
    /// Internal name: `stat_def_dex_memory`
    IsImmuneToDistillSpeed = 0x00CB,
    /// Internal name: `stat_def_move_memory`
    IsImmuneToDistillUnused = 0x00CC,
    /// Internal name: `stat_def_ability_memory`
    IsImmuneToDistillAbility = 0x00CD,
    /// Internal name: `stat_def_dodge`
    IsImmuneToShield = 0x00CE,
    /// Internal name: `stat_def_defend`
    IsImmuneToBoost = 0x00CF,
    /// Internal name: `stat_def_relife`
    IsImmuneToAutoLife = 0x00D0,
    /// Internal name: `stat_def_blow`
    IsImmuneToEject = 0x00D1,
    /// Internal name: `stat_def_curse`
    IsImmuneToCurse = 0x00D2,
    /// Internal name: `stat_def_defense`
    IsImmuneToDefend = 0x00D3,
    /// Internal name: `stat_def_protect`
    IsImmuneToGuard = 0x00D4,
    /// Internal name: `stat_def_iron`
    IsImmuneToSentinel = 0x00D5,
    /// Internal name: `stat_def_death_sentence`
    IsImmuneToDoom = 0x00D6,
    /// Internal name: `stat_hp_list`
    ///
    /// Whether the actor is visible on the frontline party list showing HP, MP, and overdrive charge
    IsVisibleOnFrontlineStats = 0x00D7,
    /// Internal name: `stat_visible_cam`
    VisibleCam = 0x00D8, //TODO: Figure out what this is and name it
    /// Internal name: `stat_visible_out`
    VisibleOut = 0x00D9, //TODO: Figure out what this is and name it
    /// Internal name: `stat_round`
    Round = 0x00DA, //TODO: Figure out what this is and name it
    /// Internal name: `stat_round_return`
    RoundReturn = 0x00DB, //TODO: Figure out what this is and name it
    /// Internal name: `stat_win_pose`
    WinPose = 0x00DC, //TODO: Figure out what this is and name it
    /// Internal name: `stat_vigor`
    Vigor = 0x00DD, //TODO: Figure out what this is and name it
    /// Internal name: `stat_fast_model_flag`
    FastModelFlag = 0x00DE, //TODO: Figure out what this is and name it
    /// Internal name: `stat_alive_not_stone`
    IsAliveAndNotPetrified = 0x00DF, //TODO: Test this
    /// Internal name: `stat_command_type`
    CommandType = 0x00E0, //TODO: Figure out what this is and name it
    /// Internal name: `stat_effect_target_flag`
    EffectTargetFlag = 0x00E1, //TODO: Figure out what this is and name it
    /// Internal name: `stat_magic_effect_ground`
    MagicEffectGround = 0x00E2, //TODO: Figure out what this is and name it
    /// Internal name: `stat_magic_effect_water`
    MagicEffectWater = 0x00E3, //TODO: Figure out what this is and name it
    /// Internal name: `stat_idle2_prob`
    Idle2Prob = 0x00E4, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_motion_type`
    AttackMotionType = 0x00E5, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_inc_speed`
    AttackIncSpeed = 0x00E6, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_dec_speed`
    AttackDecSpeed = 0x00E7, //TODO: Figure out what this is and name it
    /// Internal name: `stat_ctb`
    ///
    /// Usually used as `Ctb` in other contexts
    CurrentTurnDelay = 0x00E8,
    /// Internal name: `stat_appear_count`
    AppearCount = 0x00E9, //TODO: Figure out what this is and name it
    /// Internal name: `stat_motion_num`
    MotionNum = 0x00EA, //TODO: Figure out what this is and name it
    /// Internal name: `stat_info_mes_id`
    HelpMessageId = 0x00EB, //TODO: Test this
    /// Internal name: `stat_live_mes_id`
    ScanMessageId = 0x00EC, //TODO: Test this
    /// Internal name: `stat_visible_eff`
    VisibleEff = 0x00ED, //TODO: Figure out what this is and name it
    //? CanDisposeOfAnim?
    /// Internal name: `stat_motion_dispose_flag`
    MotionDisposeFlag = 0x00EE, //TODO: Figure out what this is and name it
    //? CanDisposeOfModel?
    /// Internal name: `stat_model_dispose_flag`
    ModelDisposeFlag = 0x00EF, //TODO: Figure out what this is and name it
    /// Internal name: `stat_def_ctb`
    DelayResistance = 0x00F0, //TODO: Test this
    /// Internal name: `stat_shadow`
    Shadow = 0x00F1, //TODO: Figure out what this is and name it
    /// Internal name: `stat_death`
    Death = 0x00F2, //TODO: Figure out what this is and name it
    /// Internal name: `stat_death_stone`
    DeathStone = 0x00F3, //TODO: Figure out what this is and name it
    /// Internal name: `stat_check_pos`
    CheckPos = 0x00F4, //TODO: Figure out what this is and name it
    /// Internal name: `stat_win_se`
    WinSe = 0x00F5, //TODO: Figure out what this is and name it
    /// Internal name: `stat_attack_num`
    AttackNum = 0x00F6, //TODO: Figure out what this is and name it
    /// Internal name: `stat_near_motion`
    NearMotion = 0x00F7, //TODO: Figure out what this is and name it
    //? What stat is increased by talking to it? Check Seymour
    /// Internal name: `stat_talk_stat1`
    TalkStat1 = 0x00F8, //TODO: Figure out what this is and name it
    //? What other stat is increased by talking to it? Check Seymour
    /// Internal name: `stat_talk_stat2`
    TalkStat2 = 0x00F9, //TODO: Figure out what this is and name it
    /// Internal name: `stat_near_motion_set`
    ForceCloseRangeAttackAnim = 0x00FA, //TODO: Test this
    //? Base animation speed?
    /// Internal name: `stat_motion_speed_normal`
    MotionSpeedNormal = 0x00FB, //TODO: Figure out what this is and name it
    //? Initial animation speed?
    /// Internal name: `stat_motion_speed_start`
    MotionSpeedNormalStart = 0x00FC, //TODO: Figure out what this is and name it
    /// Internal name: `stat_own_attack_near`
    OwnAttackNear = 0x00FD, //TODO: Figure out what this is and name it
    //? What other other stat is increased by talking to it? Check Seymour
    /// Internal name: `stat_talk_stat3`
    TalkStat3 = 0x00FE, //TODO: Figure out what this is and name it
    /// Internal name: `stat_command_set`
    CommandSet = 0x00FF, //TODO: Figure out what this is and name it
    /// Internal name: `stat_prov_command_flag`
    RetainsControlWhenProvoked = 0x0100, //TODO: Test this
    /// Internal name: `stat_prov_chr`
    Provoker = 0x0101,
    /// Internal name: `stat_use_mp0`
    Spellspring = 0x0102,
    /// Internal name: `stat_icon_number`
    CtbIcon = 0x0103, //TODO: Test this and document icons in an enum or list somewhere
    /// Internal name: `stat_sound_hit_num`
    SoundHitNum = 0x0104, //TODO: Figure out what this is and name it
    /// Internal name: `stat_damage_num_pos`
    DamageNumPos = 0x0105, //TODO: Figure out what this is and name it
    //? Who summoned it to manipulate the position of in the summon anim?
    /// Internal name: `stat_summoner`
    Summoner = 0x0106, //TODO: Figure out what this is and name it
    /// Internal name: `stat_sp_invincible`
    IsInvincible = 0x0107,
    /// Internal name: `stat_sp_inv_magic`
    IsImmuneToMagic = 0x0108,
    /// Internal name: `stat_sp_inv_physic`
    IsImmuneToPhysical = 0x0109,
    /// Internal name: `stat_blue_magic`
    CanTeachRonsoRage = 0x010A,
    /// Internal name: `stat_sp_disable_zan`
    IsImmuneToZanmato = 0x010B, //TODO: Test this
    /// Internal name: `stat_over_kill_hp`
    OverkillThreshold = 0x010C,
    /// Internal name: `stat_return_motion_type`
    ReturnMotionType = 0x010D, //TODO: Figure out what this is and name it
    /// Internal name: `stat_cam_width`
    CamWidth = 0x010E, //TODO: Figure out what this is and name it
    /// Internal name: `stat_cam_height`
    CamHeight = 0x010F, //TODO: Figure out what this is and name it
    /// Internal name: `stat_height`
    Height = 0x0110, //TODO: Figure out what this is and name it
    /// Internal name: `stat_youjinbo`
    YojimboCompatibility = 0x0111,
    /// Internal name: `stat_payment`
    YojimboGivenGil = 0x0112,
    /// Internal name: `stat_monster_value_max`
    ZanmatoLevel = 0x0113,
    /// Internal name: `stat_command_exe_count`
    TurnsTaken = 0x0114,
    /// Internal name: `stat_consent`
    YojimboReaction = 0x0115,
    /// Internal name: `stat_attack_near_frame`
    AttackNearFrame = 0x0116, //TODO: Figure out what this is and name it
    /// Internal name: `stat_energy`
    MagusSisterMotivation = 0x0117,
    //? Seymour's Anima or Isaaru's Bahamut might use this?
    /// Internal name: `stat_limit_gauge_add`
    LimitGaugeAdd = 0x0118, //TODO: Figure out what this is and name it
    /// Internal name: `stat_hp_half`
    IsNearDeath = 0x0119,
    /// Internal name: `stat_limit_gauge_check`
    HasOverdriveAvailable = 0x011A, //TODO: Test this
    /// Internal name: `stat_hp_check`
    HpCheck = 0x011B, //TODO: Figure out what this is and name it
    /// Internal name: `stat_mp_check`
    MpCheck = 0x011C, //TODO: Figure out what this is and name it
    /// Internal name: `stat_ba_all_check`
    HasNulAll = 0x011D, //TODO: Test this
    /// Internal name: `stat_shell_reflect`
    ShellReflect = 0x011E, //TODO: Figure out what this is and name it
    /// Internal name: `stat_protess_reflect`
    ProtectReflect = 0x011F, //TODO: Figure out what this is and name it
    /// Internal name: `stat_haste_reflect`
    HasteReflect = 0x0120, //TODO: Figure out what this is and name it
    //? Whether or not it uses the low hp animations?
    /// Internal name: `stat_weak_motion`
    WeakMotion = 0x0121, //TODO: Figure out what this is and name it
    // Can just remove testing the next one as I'm reasonably sure that's the case, but testing is preferable
    /// Internal name: `stat_sp_wairo`
    IsImmuneToBribe = 0x0122, //TODO: Test this
    /// Internal name: `stat_attack_motion_frame`
    AttackMotionFrame = 0x0123, //TODO: Figure out what this is and name it
    /// Internal name: `stat_motion_type_reset`
    MotionTypeReset = 0x0124, //TODO: Figure out what this is and name it
    /// Internal name: `stat_motion_type_add`
    MotionTypeAdd = 0x0125, //TODO: Figure out what this is and name it
    /// Internal name: `stat_death_status`
    DeathStatus = 0x0126, //TODO: Figure out what this is and name it
    /// Internal name: `stat_target_list`
    TargetList = 0x0127, //TODO: Figure out what this is and name it
    //? Position of overdrive gauge for vs aeon battles etc.?
    /// Internal name: `stat_limit_bar_pos`
    LimitBarPos = 0x0128, //TODO: Figure out what this is and name it
    /// Internal name: `stat_center_chr_flag`
    IsCenterCharacter = 0x0129, //TODO: Figure out what this is and name it better
    //? Has same target as another enemy maybe?
    /// Internal name: `stat_same_target_check`
    SameTargetCheck = 0x012A, //TODO: Figure out what this is and name it
    /// Internal name: `stat_get_ap`
    ApRewardNormal = 0x012B,
    /// Internal name: `stat_get_over_ap`
    ApRewardOverkill = 0x012C,
    /// Internal name: `stat_get_gil`
    GilReward = 0x012D,
    //TODO: What are these? For Aeon stats? Also make sure they're in order
    /// Internal name: `stat_str_up`
    BonusStrength = 0x012E,
    /// Internal name: `stat_vit_up`
    BonusDefense = 0x012F,
    /// Internal name: `stat_mag_up`
    BonusMagic = 0x0130,
    /// Internal name: `stat_spirit_up`
    BonusMagicDefense = 0x0131,
    /// Internal name: `stat_dex_up`
    BonusAgility = 0x0132,
    /// Internal name: `stat_luck_up`
    BonusLuck = 0x0133,
    /// Internal name: `stat_avoid_up`
    BonusEvasion = 0x0134,
    /// Internal name: `stat_hit_up`
    BonusAccuracy = 0x0135,
    //? Spellspring for monsters?
    /// Internal name: `stat_use_mp`
    UseMp = 0x0136, //TODO: Figure out what this is and name it
    /// Internal name: `stat_use_limit`
    UseLimit = 0x0137, //TODO: Figure out what this is and name it
    /// Internal name: `stat_use_limit_all`
    UseLimitAll = 0x0138, //TODO: Figure out what this is and name it
    /// Internal name: `stat_continue_magic`
    IsMulticasting = 0x0139, //TODO: Figure out what this is and name it
    /// Internal name: `stat_item1_com`
    Drop1CommonType = 0x013A, //TODO: Test this
    /// Internal name: `stat_item1_rare`
    Drop1RareType = 0x013B, //TODO: Test this
    /// Internal name: `stat_item2_com`
    Drop2CommonType = 0x013C, //TODO: Test this
    /// Internal name: `stat_item2_rare`
    Drop2RareType = 0x013D, //TODO: Test this
    /// Internal name: `stat_item1_com_over_kill`
    Drop1OverkillCommonType = 0x013E, //TODO: Test this
    /// Internal name: `stat_item1_rare_over_kill`
    Drop1OverkillRareType = 0x013F, //TODO: Test this
    /// Internal name: `stat_item2_com_over_kill`
    Drop2OverkillCommonType = 0x0140, //TODO: Test this
    /// Internal name: `stat_item2_rare_over_kill`
    Drop2OverkillRareType = 0x0141, //TODO: Test this
    /// Internal name: `stat_item1_com_num`
    Drop1CommonCount = 0x0142, //TODO: Test this
    /// Internal name: `stat_item1_rare_num`
    Drop1RareCount = 0x0143, //TODO: Test this
    /// Internal name: `stat_item2_com_num`
    Drop2CommonCount = 0x0144, //TODO: Test this
    /// Internal name: `stat_item2_rare_num`
    Drop2RareCount = 0x0145, //TODO: Test this
    /// Internal name: `stat_item1_com_over_kill_num`
    Drop1OverkillCommonCount = 0x0146, //TODO: Test this
    /// Internal name: `stat_item1_rare_over_kill_num`
    Drop1OverkillRareCount = 0x0147, //TODO: Test this
    /// Internal name: `stat_item2_com_over_kill_num`
    Drop2OverkillCommonCount = 0x0148, //TODO: Test this
    /// Internal name: `stat_item2_rare_over_kill_num`
    Drop2OverkillRareCount = 0x0149, //TODO: Test this
    /// Internal name: `stat_death_return`
    DeathReturn = 0x014A, //TODO: Figure out what this is and name it
    /// Internal name: `stat_linear_move_reset`
    LinearMoveReset = 0x014B, //TODO: Figure out what this is and name it
    /// Internal name: `stat_bodyhit_direct`
    BodyHitDirect = 0x014C, //TODO: Figure out what this is and name it
    //? Aeons?
    /// Internal name: `stat_join`
    IsRecruited = 0x014D, //TODO Test this
    /// Internal name: `stat_eternal_relife`
    PermanentAutoLife = 0x014E,
    //? RedirectAttacksToNeck?
    /// Internal name: `stat_neck_target_flag`
    NeckTargetFlag = 0x014F, //TODO: Figure out what this is and name it
    /// Internal name: `stat_visible_out_on`
    VisibleOutOn = 0x0150, //TODO: Figure out what this is and name it
    //? TakesDmgFromRegen?
    /// Internal name: `stat_regen_damage_flag`
    RegenDmgFlag = 0x0151, //TODO: Figure out what this is and name it
    /// Internal name: `stat_num_print_element`
    NumPrintElement = 0x0152, //TODO: Figure out what this is and name it
}

#[repr(u16)]
pub enum MoveProperty {
    DmgFormula = 0x0,
    DmgType = 0x1,
    AffectsHp = 0x2,
    AffectsMp = 0x3,
    /// Inflicts Delay
    AffectsCtb = 0x4,
    Holystrike = 0x5,
    Waterstrike = 0x6,
    Thunderstrike = 0x7,
    Icestrike = 0x8,
    Firestrike = 0x9,
    TargetType = 0xA,
}
