use bitflags::bitflags;

use crate::{
    battle::DamageFormula,
    general::{Character, Elements},
    menu::{OverdriveCategory, SubMenu},
    sphere_grid::SphereRole,
};

bitflags! {
    /// Flags determining what the move targets
    pub struct TargetFlags: u8 {
        /// Whether the move can target anything
        const enabled = 0x01;
        /// Whether the move can target enemies
        const enemies = 0x02;
        /// Whether the move targets multiple actors at once
        const multi = 0x04;
        /// Whether the move can only be used on the user
        const self_only = 0x08;
        const flag5 = 0x10; // TODO: Analyze
        /// Whether the move can target either team
        const either_team = 0x20;
        /// Whether the move can target dead characters
        const dead = 0x40;
        const flag8 = 0x80; // Maybe long-range? //TODO: Analyze
    }

    /// Properties of the command in the battle menu
    pub struct MenuPropFlags: u8 {
        /// Whether the command appears as top-level in the menu
        const top_level_in_menu = 0x01;
        /// Whether the command opens a sub-menu
        const opens_sub_menu = 0x10;
    }

    /// Miscellaneous properties of moves
    ///
    /// u24 in all structures but we work around in on loading and saving
    pub struct MiscProps: u32 {
        /// Whether the move can be used outside of combat
        const usable_outside_combat = 0x000001;
        /// Whether the move can be used inside of combat
        const usable_in_combat = 0x000002;
        /// Whether the move name is displayed in the help bar when it's used
        const display_move_name = 0x000004;
        const flag4 = 0x000008;
        /// Whether the move can miss
        const can_miss = 0x000010;
        const flag6 = 0x000020;
        /// Whether the move's accuracy is affected by <span class="status">Darkness</span>
        const affected_by_darkness = 0x000040;
        /// Whether the move can be reflected using <span class="status">Reflect</span>
        const can_be_reflected = 0x000080;
        /// Whether the move absorbs damage?
        const absorb_dmg = 0x000100;
        /// Whether the move attemps to steal items
        const steal_item = 0x000200;
        /// Whether the move appears in the Use menu
        const in_use_menu = 0x000400;
        /// Whether the move appears in the sub menu (right)
        const in_sub_menu = 0x000800;
        /// Whether the move appears in the trigger menu (left)
        const in_trigger_menu = 0x001000;
        /// Whether the move inflicts weak delay
        const inflict_delay_weak = 0x002000;
        /// Whether the move inflicts strong delay
        const inflict_delay_strong = 0x004000;
        /// Whether the move targets random enemies
        const random_targets = 0x008000;
        /// Whether the move ignores target defense
        const is_piercing = 0x010000;
        /// Whether the move is blocked by silence
        const affect_by_silence = 0x020000;
        /// Whether the move cares about weapon properties
        const uses_weapon_props = 0x040000;
        /// Whether the move is a trigger command
        const is_trigger_command = 0x080000;
        /// Whether the move uses a tier 1 casting animation (Fire / Water / Thunder / Blizzard)
        const use_tier1_cast_anim = 0x100000;
        /// Whether the move uses a tier 3 casting animation (Firaga / Waterga / Thundaga / Blizzaga)
        const use_tier3_cast_anim = 0x200000;
        /// Whether the move removes the user from battle (like Self-Destruct)
        const destroy_user = 0x400000;
        /// Whether the move will miss alive actors (like Phoenix Down)
        const miss_if_alive = 0x800000;
    }

    /// Properties of animations used
    pub struct AnimProps: u8 {
        const flag1 = 0x01;
        const flag2 = 0x02; // Only set on Entrust //TODO: Analyze what happens when set on other moves have this
        const flag3 = 0x04; // maybe whether it uses the casting anim
        /// Move user runs off the screen
        const user_runs_off = 0x08;
        const flag5 = 0x10; // maybe show spell aura
        const flag6 = 0x20;
        const flag7 = 0x40; // set on all Aeon overdrives. `hasSpecialCutscene`? //TODO: Analyze
        const flag8 = 0x80; // Only set on Bribe //TODO: Analyze what happens when set on other moves
    }

    /// Properties of damage dealt
    pub struct DmgProps: u8 {
        /// Whether the damage is physical
        const is_physical = 0x01;
        /// Whether the damage is magical
        const is_magical = 0x02;
        /// Whether the move can result in a critical hit
        const can_crit = 0x04;
        /// Whether the move uses the equipped weapon's crit bonus
        const uses_wpn_crit_bonus = 0x08;
        /// Whether the move heals
        const is_healing = 0x10;
        /// Whether the move removes status
        const is_cleansing = 0x20; // i.e. Esuna, Remedy
        ///  Whether the move cannot break through damage limit
        const supresses_bdl = 0x40;
        /// Whether the move breaks through damage limit
        const break_dmg_limit = 0x80;
    }

    /// Determines what the move affects outside of battle
    pub struct PartyPreviewProps: u8 {
        /// Whether the move uses the party preview
        const uses_preview = 0x01;
        /// Whether the move affects MP
        const heals_mp = 0x02;
        /// Whether the move affects status effects
        const heal_status = 0x04;
        /// Whether the move affects HP
        const heal_hp = 0x08;
    }

    /// Determines what the move affects
    pub struct DamageClass: u8 {
        /// Whether the move affects HP
        const hp = 0x01;
        /// Whether the move affects MP
        const mp = 0x02;
        /// Whether the move affects turn delay
        const ctb = 0x04;
        const flag4 = 0x08;
    }

    /// Determines what special status effects the move inflicts
    pub struct InflictSpecialStatusFlags: u16 {
        /// Whether the move inflicts <span class="status">Scan</span>
        const scan = 0x0001;
        /// Whether the move inflicts <span class="status">Distill Power</span>
        const distill_power = 0x0002;
        /// Whether the move inflicts <span class="status">Distill Mana</span>
        const distill_mana = 0x0004;
        /// Whether the move inflicts <span class="status">Distill Speed</span>
        const distill_speed = 0x0008;
        /// Whether the move inflicts the unused `move_memory`
        const unused1 = 0x0010;
        /// Whether the move inflicts <span class="status">Distill Ability</span>
        const distill_ability = 0x0020;
        /// Whether the move inflicts the aeon status <span class="status">Shield</span>
        const shield = 0x0040;
        /// Whether the move inflicts the aeon status <span class="status">Boost</span>
        const boost = 0x0080;
        /// Whether the move ejects the target
        const eject = 0x0100;
        /// Whether the move inflicts <span class="status">Auto-Life</span>
        const auto_life = 0x0200;
        /// Whether the move inflicts <span class="status">Curse</span>
        const curse = 0x0400;
        /// Whether the move inflicts the <span class="status">Defend</span> status
        const defend = 0x0800;
        /// Whether the move makes the target defend others using <span class="status">Guard</span>
        const guard = 0x1000;
        /// Whether the move makes the target defend others using <span class="status">Sentinel</span>
        const sentinel = 0x2000;
        /// Whether the move inflicts <span class="status">Doom</span>
        const doom = 0x4000;
        const unused2 = 0x8000;
    }

    /// Determine what stat buffs the move inflicts
    pub struct StatBuffFlags: u8 {
        /// Whether the move raises Strength and Defense
        const cheer = 0x01;
        /// Whether the move raises Accuracy
        const aim = 0x02;
        /// Whether the move raises Magic and Magic Defense
        const focus = 0x04;
        /// Whether the move raises Evasion
        const reflex = 0x08;
        /// Whether the move raises Luck
        const luck = 0x10;
        /// Whether the move lowers Luck
        const jinx = 0x20;
        const unused1 = 0x40;
        const unused2 = 0x80;
    }

    /// Determines what special buffs the move inflicts
    pub struct SpecialBuffFlags: u8 {
        /// Whether the move doubles the target's Max HP
        const double_hp = 0x01;
        /// Whether the move doubles the target's Max MP
        const double_mp = 0x02;
        /// Whether the move allows the target to cast spells for free
        const spellspring = 0x04;
        /// Whether the move makes the target always deal at least 9999 damage
        const dmg_9999 = 0x08;
        /// Whether the move makes all of the target's attacks critical hits
        const always_crit = 0x10;
        /// Whether the move boosts the target's overdrive charge gain by 150%
        ///
        /// Stacks with `overdrive_200`
        const overdrive_150 = 0x20;
        /// Whether the move boosts the target's overdrive charge gain by 200%
        ///
        /// Stacks with `overdrive_150`
        const overdrive_200 = 0x40;
        const unused1 = 0x80;
    }
}

pub struct Status {
    /// Chance to inflict <span class="status">Death</span>
    pub chance_death: u8,
    /// Chance to inflict <span class="status">Zombie</span>
    pub chance_zombie: u8,
    /// Chance to inflict <span class="status">Petrified</span>
    pub chance_petrify: u8,
    /// Chance to inflict <span class="status">Poison</span>
    pub chance_poison: u8,
    /// Chance to inflict <span class="status">Power Break</span>
    pub chance_power_break: u8,
    /// Chance to inflict <span class="status">Magic Break</span>
    pub chance_magic_break: u8,
    /// Chance to inflict <span class="status">Armor Break</span>
    pub chance_armor_break: u8,
    /// Chance to inflict <span class="status">Mental Break</span>
    pub chance_mental_break: u8,
    /// Chance to inflict <span class="status">Confusion</span>
    pub chance_confuse: u8,
    /// Chance to inflict <span class="status">Berserk</span>
    pub chance_berserk: u8,
    /// Chance to inflict <span class="status">Provoke</span>
    pub chance_provoke: u8,
    /// Chance to inflict <span class="status">Threaten</span>
    pub chance_threaten: u8,
    /// Chance to inflict <span class="status">Sleep</span>
    pub chance_sleep: u8,
    /// Chance to inflict <span class="status">Silence</span>
    pub chance_silence: u8,
    /// Chance to inflict <span class="status">Darkness</span>
    pub chance_darkness: u8,
    /// Chance to inflict <span class="status">Shell</span>
    pub chance_shell: u8,
    /// Chance to inflict <span class="status">Protect</span>
    pub chance_protect: u8,
    /// Chance to inflict <span class="status">Reflect</span>
    pub chance_reflect: u8,
    /// Chance to inflict <span class="status">NulTide</span>
    pub chance_nul_water: u8,
    /// Chance to inflict <span class="status">NulBlaze</span>
    pub chance_nul_fire: u8,
    /// Chance to inflict <span class="status">NulShock</span>
    pub chance_nul_thunder: u8,
    /// Chance to inflict <span class="status">NulFrost</span>
    pub chance_nul_ice: u8,
    /// Chance to inflict <span class="status">Regen</span>
    pub chance_regen: u8,
    /// Chance to inflict <span class="status">Haste</span>
    pub chance_haste: u8,
    /// Chance to inflict <span class="status">Span</span>
    pub chance_slow: u8,

    /// Amount of turns the <span class="status">Sleep</span> should last for
    pub dur_sleep: u8,
    /// Amount of turns the <span class="status">Silence</span> should last for
    pub dur_silence: u8,
    /// Amount of turns the <span class="status">Darkness</span> should last for
    pub dur_darkness: u8,
    /// Amount of turns the <span class="status">Shell</span> should last for
    pub dur_shell: u8,
    /// Amount of turns the <span class="status">Protect</span> should last for
    pub dur_protect: u8,
    /// Amount of turns the <span class="status">Reflect</span> should last for
    pub dur_reflect: u8,
    /// Amount of resisted attacks the <span class="status">NulTide</span> should last for
    pub dur_nul_water: u8,
    /// Amount of resisted attacks the <span class="status">NulBlaze</span> should last for
    pub dur_nul_fire: u8,
    /// Amount of resisted attacks the <span class="status">NulShock</span> should last for
    pub dur_nul_thunder: u8,
    /// Amount of resisted attacks the <span class="status">NulFrost</span> should last for
    pub dur_nul_ice: u8,
    /// Amount of turns the <span class="status">Regen</span> should last for
    pub dur_regen: u8,
    /// Amount of turns the <span class="status">Haste</span> should last for
    pub dur_haste: u8,
    /// Amount of turns the <span class="status">Slow</span> should last for
    pub dur_slow: u8,
}

/// The Command structure is used in `command.bin`, `monmagic1.bin`, `monmagic2.bin`, and `item.bin`
#[doc(alias("item", "monmagic"))]
pub struct Command {
    name: String,
    unk_0x02: u8,
    unk_0x03: u8,
    unk_0x06: u8,
    unk_0x07: u8,
    description: String,
    unk_0x0a: u8,
    unk_0x0b: u8,
    other_text: String,
    unk_0x0e: u8,
    unk_0x0f: u8,
    anim1: u16,
    anim2: u16,
    icon: u8,
    caster_anim: u8,
    menu_props: MenuPropFlags,
    sub_menu_cat_2: SubMenu,
    sub_menu_cat: SubMenu,
    chr_user: Character,
    target_flags: TargetFlags,
    unk_props: u8,
    misc_props: MiscProps,
    anim_props: AnimProps,
    dmg_props: DmgProps,
    steal_gil: bool,
    party_preview: bool,
    dmg_class: DamageClass,
    move_rank: u8,
    mp_cost: u8,
    overdrive_cost: u8,
    crit_bonus: u8,
    dmg_formula: DamageFormula,
    accuracy: u8,
    power: u8,
    hit_count: u8,
    shatter_chance: u8,
    elements: Elements,
    status: Status,
    stat_buffs: StatBuffFlags,
    unk_0x57: u8,
    overdrive_cat: OverdriveCategory,
    stat_buffs_value: u8,
    special_buffs: SpecialBuffFlags,
    unk_0x5b: u8,
    ordering_idx_in_menu: u8,
    sphere_grid_role: SphereRole,
    unk_0x5e: u8,
    unk_0x5f: u8,
}
