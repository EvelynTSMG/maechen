use bitflags::bitflags;

use crate::{
    general::Elements,
    structs::battle_actor::{AutoStatus, ExtraImmunities, Stats, Status},
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

bitflags!(
    #[derive(Debug)]
    #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
    pub struct MonProps: u16 {
        const armored =                 0x001;
        const immune_fractional_dmg =   0x002;
        const immune_life =             0x004;
        const immune_sensor =           0x008;
        const unk1 =                    0x010;
        const immune_phys_dmg =         0x020;
        const immune_mag_dmg =          0x040;
        const unk2 =                    0x080;
        const immune_ctb_dmg =          0x100;
    }
);

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
/// Chunk in monster files containing statistics
pub struct MonStatsChunk {
    pub name: String,
    pub help_text: String,
    pub scan_text: String,
    pub hp: u32,
    pub mp: u32,
    pub overkill: u32,
    pub stats: Stats,
    pub props: MonProps,
    pub poison_dmg: u8,
    pub absorbs: Elements,
    pub immune_to: Elements,
    pub resists: Elements,
    pub weak_to: Elements,
    pub status_resist: Status,
    pub auto_status: AutoStatus,
    pub extra_immunities: ExtraImmunities,
    pub forced_move: u16,
    pub monster_idx: u16,
    pub model_idx: u16,
    pub doom_counter: u8,
    pub monster_arena_idx: u16,
    pub unk: u16,
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Spoils {
    pub item_primary_common: u16,
    pub item_primary_rare: u16,
    pub item_secondary_common: u16,
    pub item_secondary_rare: u16,
    pub amount_primary_common: u8,
    pub amount_primary_rare: u8,
    pub amount_secondary_common: u8,
    pub amount_secondary_rare: u8,
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct StealSpoils {
    pub item_common: u16,
    pub item_rare: u16,
    pub amount_common: u8,
    pub amount_rare: u8,
    pub item_bribe: u16,
    pub amount_bribe: u8,
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct GearAbilities {
    pub weapon_abilities: [u16; 8],
    pub armor_abilities: [u16; 8],
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct GearSpoils {
    pub slot_count: u8,
    pub formula: u8,
    pub crit: u8,
    pub power: u8,
    pub ability_count: u8,
    pub abilities: [GearAbilities; 7],
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct MonSpoilsChunk {
    pub gil: u16,
    pub ap: u16,
    pub ap_overkill: u16,
    pub ronso_rage: u16,
    pub drop_chance_primary: u8,
    pub drop_chance_secondary: u8,
    pub steal_chance: u8,
    pub drop_chance_gear: u8,
    pub drop_normal: Spoils,
    pub drop_overkill: Spoils,
    pub steal_spoils: StealSpoils,
    pub gear_spoils: GearSpoils,
    pub zanmato_level: u8,
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct MonsterFile {
    // script chunk
    // script data chunk
    pub stats: MonStatsChunk,
    pub spoils: MonSpoilsChunk,
    // chunk5
    pub stats_en: MonStatsChunk,
}