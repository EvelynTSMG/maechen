use bitflags::bitflags;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

bitflags!(
    #[derive(Debug)]
    #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
    /// Bitfield of elements
    pub struct Elements: u8 {
        const fire = 0x01;
        const ice = 0x02;
        const thunder = 0x04;
        const water = 0x08;
        const holy = 0x10;
        const flag6 = 0x20;
        const flag7 = 0x40;
        const flag8 = 0x80;
    }
);

#[repr(u8)]
/// Possible input shift amounts the game performs on the input bitfield
pub enum Input {
    Confirm = 0x05,
    Up = 0x12,
    Right = 0x13,
    Down = 0x14,
    Left = 0x15,
}

#[repr(u8)]
/// Joystick Axis
pub enum Axis {
    Axis1,
    Axis2,
}

#[repr(u16)]
pub enum Area {
    /// Internal name: `system`
    System = 0x0000,
    /// Internal name: `test00`
    Test00 = 0x0002,
    /// Internal name: `test10`
    Test10 = 0x0005,
    /// Internal name: `test11`
    Test11 = 0x0006,
    /// Internal name: `tori`
    TestToriyama = 0x0262,
    /// Internal name: `znkd`
    DreamZanarkand = 0x000A,
    /// Internal name: `bjyt`
    BaajTemple = 0x001E,
    /// Internal name: `cdsp`
    ///
    /// Al Bhed Ship & Underwater Ruins
    CidShip = 0x0032,
    /// Internal name: `bsil`
    Besaid = 0x0041,
    /// Internal name: `slik`
    ///
    /// S.S. Liki
    ShipLiki = 0x005F,
    /// Internal name: `klyt`
    ///
    /// Kilika Temple and Woods
    Kilika = 0x0083,
    /// Internal name: `lchb`
    Luca = 0x00A5,
    /// Internal name: `mihn`
    ///
    /// Mi'ihen Highroad, Oldroad, and Newroad
    Miihen = 0x00D2,
    /// Internal name: `kino`
    MushroomRock = 0x00DC,
    /// Internal name: `genk`
    Moonflow = 0x00F5,
    /// Internal name: `kami`
    ThunderPlains = 0x012C,
    /// Internal name: `mcfr`
    MacalaniaForest = 0x0136,
    /// Internal name: `maca`
    MacalaniaLake = 0x014A,
    /// Internal name: `mcyt`
    MacalaniaTemple = 0x0154,
    /// Internal name: `bika`
    Bikanel = 0x015E,
    /// Internal name: `azit`
    AlBhedHome = 0x0168,
    /// Internal name: `hiku`
    Airship = 0x017C,
    /// Internal name: `stbv`
    Bevelle = 0x0195,
    /// Internal name: `bvyt`
    BevelleTemple = 0x019A,
    /// Internal name: `nagi`
    ///
    /// Calm Lands and Cavern of the Stolen Fayth
    CalmLands = 0x01A9,
    /// Internal name: `lmyt`
    RemiemTemple = 0x01BD,
    /// Internal name: `mtgz`
    MtGagazet = 0x01E5,
    /// Internal name: `zkrn`
    ZanarkandRuins = 0x01F4,
    /// Internal name: `dome`
    ZanarkandDome = 0x0203,
    /// Internal name: `ssbt`
    SinBattle = 0x0235,
    /// Internal name: `sins`
    InsideSin = 0x0244,
    /// Internal name: `omeg`
    OmegaRuins = 0x024E,
    /// Internal name: `zzzz`
    BattleSim = 0x0259,
}

#[repr(u8)]
pub enum Character {
    /// Internal name: `PC_TIDUS`
    Tidus = 0x00,
    /// Internal name: `PC_YUNA`
    Yuna = 0x01,
    /// Internal name: `PC_AURON`
    Auron = 0x02,
    /// Internal name: `PC_KIMAHRI`
    Kimahri = 0x03,
    /// Internal name: `PC_WAKKA`
    Wakka = 0x04,
    /// Internal name: `PC_LULU`
    Lulu = 0x05,
    /// Internal name: `PC_RIKKU`
    Rikku = 0x06,
    /// Internal name: `PC_SEYMOUR`
    Seymour = 0x07,
    /// Internal name: `PC_VALEFOR`
    Valefor = 0x08,
    /// Internal name: `PC_IFRIT`
    Ifrit = 0x09,
    /// Internal name: `PC_IXION`
    Ixion = 0x0A,
    /// Internal name: `PC_SHIVA`
    Shiva = 0x0B,
    /// Internal name: `PC_BAHAMUT`
    Bahamut = 0x0C,
    /// Internal name: `PC_ANIMA`
    Anima = 0x0D,
    /// Internal name: `PC_YOJIMBO`
    Yojimbo = 0x0E,
    /// Internal name: `PC_MAGUS1`
    Cindy = 0x0F,
    /// Internal name: `PC_MAGUS2`
    Sandy = 0x10,
    /// Internal name: `PC_MAGUS3`
    Mindy = 0x11,
    /// Internal name: `PC_DUMMY`
    Dummy = 0x12,
    /// Internal name: `PC_DUMMY2`
    Dummy2 = 0x13,
}

#[repr(u8)]
/// Level upgrades for celestial weapons
pub enum CelestialLevel {
    Basic = 0x0,
    Half = 0x1,
    Full = 0x2,
}
