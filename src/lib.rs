use anim::{DeathAnim, YojimboReaction};
use audio::{Bgm, Sfx};
use battle::{
    ActorProperty, AmbushState, BattleEndType, BattleTrans, DamageFormula, DamageType, MoveProperty,
};
use general::{Area, Axis, CelestialLevel, Character, Input};
use menu::{OverdriveCategory, SubMenu};
use script::{Selector, Target, TargetType};
use sphere_grid::{GridType, SphereRole, SphereType};
use text::TextAlign;

pub mod anim;
pub mod audio;
pub mod battle;
pub mod general;
pub mod menu;
pub mod script;
pub mod sphere_grid;
pub mod text;

pub mod files;
pub mod structs;

macro_rules! enum_vars {
    (
        $enum_name:ident .
        $var_name:ident :
        $ename:ident {
            $( // repeat variations
                $evariant:ident$(,)?
            )+
        }
    ) => (
        match $var_name {
            $( // for each: variant
                stringify!($evariant) => $ename::$evariant as usize,
            )+
            _ => panic!("Enum '{}.{}' not found",  $enum_name, $var_name),
        }
    )
}

pub fn get_enum(enum_name: &str, variant_name: &str) -> usize {
    match enum_name {
        // general.rs
        "Input" => enum_vars!(enum_name.variant_name : Input { Confirm, Up, Right, Down, Left }),
        "Axis" => enum_vars!(enum_name.variant_name : Axis { Axis1, Axis2 }),
        "Area" => {
            enum_vars!(enum_name.variant_name : Area { System, Test00, Test10, Test11, TestToriyama, DreamZanarkand, BaajTemple, CidShip, Besaid, ShipLiki, Kilika, Luca, Miihen, MushroomRock, Moonflow, ThunderPlains, MacalaniaForest, MacalaniaLake, MacalaniaTemple, Bikanel, AlBhedHome, Airship, Bevelle, BevelleTemple, CalmLands, RemiemTemple, MtGagazet, ZanarkandRuins, ZanarkandDome, SinBattle, InsideSin, OmegaRuins, BattleSim })
        }
        "Character" => {
            enum_vars!(enum_name.variant_name : Character { Tidus, Yuna, Auron, Kimahri, Wakka, Lulu, Rikku, Seymour, Valefor, Ifrit, Ixion, Shiva, Bahamut, Anima, Yojimbo, Cindy, Sandy, Mindy, Dummy, Dummy2 })
        }
        "CelestialLevel" => {
            enum_vars!(enum_name.variant_name : CelestialLevel { Basic, Half, Full })
        }

        // battle.rs
        "AmbushState" => {
            enum_vars!(enum_name.variant_name : AmbushState { Normal, PreEmptive, Ambush, None })
        }
        "BattleTrans" => enum_vars!(enum_name.variant_name : BattleTrans { Shatter, Fade }),
        "BattleEndType" => {
            enum_vars!(enum_name.variant_name : BattleEndType { GameOver, Victory, Escape })
        }
        "DamageFormula" => {
            enum_vars!(enum_name.variant_name : DamageFormula { None, StrVsDef, StrIgnoreDef, MagVsMDef, MagIgnoreMDef, CurrentDiv16, Multiple50, Healing, MaxDiv16, Multiple50WithVariance, TicksDiv16, SpecialMag, FixedUserMaxHp, FixedChosenGil, FixedKills, Fixed9999 })
        }
        "DamageType" => {
            enum_vars!(enum_name.variant_name : DamageType { Special, Physical, Magical })
        }
        "ActorProperty" => {
            enum_vars!(enum_name.variant_name : ActorProperty { Hp, Mp, MaxHp, MaxMp, IsAlive, IsPoisoned, IsPetrified, IsZombie, IsLowHp, Strength, Defense, Magic, MagicDefense, Agility, Luck, Evasion, Accuracy, PoisonDmg, OverdriveMode, OverdriveCharge, MaxOverdriveCharge, IsFrontline, IsMale, IsFemale, IsAeon, IsEnemy, IsFlying, IsDying, BattleRow, BattleArenaPos, BattleDistance, Group, IsArmored, IsImmuneToFractionalDmg, IsImmuneToLife, IsImmuneToSensor, SpLive, IsPowerBroken, IsMagicBroken, IsArmorBroken, IsMentalBroken, IsConfused, IsBerserk, IsProvoked, IsThreatened, SleepTurnsLeft, DarknessTurnsLeft, ShellTurnsLeft, ProtectTurnsLeft, ReflectTurnsLeft, HasNulWater, HasNulFire, HasNulThunder, HasNulIce, RegenTurnsLeft, HasteTurnsLeft, SlowTurnsLeft, HasSensor, HasFirstStrike, HasInitiative, HasCounterAttack, HasEvadeAndCounter, HasDarkAttack, HasDoubleAp, HasDoubleExp, HasMagicBooster, HasMagicCounter, HasAlchemy, HasAutoPotion, HasAutoMed, HasAutoPhoenix, HasLimitUp, HasDream, HasPiercing, HasExchange, HasHpRecover, HasMpRecover, HasNoEncounter, DeathAnim, EventChr, GetsTurns, IsTargetable, IsVisibleOnCtb, IsVisible, Location1, Location2, Efflv, Model, Parent, LocationTarget, AnimsVariant, IsBoss, MoveFlag, LiveMotion, AdjustPos, HeightOn, SleepRecoverFlag, AbsorbsFire, AbsorbsIce, AbsorbsThunder, AbsorbsWater, AbsorbsHoly, IsImmuneToFire, IsImmuneToIce, IsImmuneToThunder, IsImmuneToWater, IsImmuneToHoly, ResistsFire, ResistsIce, ResistsThunder, ResistsWater, ResistsHoly, IsWeakToFire, IsWeakToIce, IsWeakToThunder, IsWeakToWater, IsWeakToHoly, AdjustPosFlag, InvPhysicMotion, InvMagicMotion, TimesStolenFrom, WaitMotionFlag, AttackReturnFlag, AttackNormalFrame, IsTough, IsHeavy, BodyHitFlag, Effvar, StealCommonItemType, StealCommonItemCount, StealRareItemType, StealRareItemCount, Magiclv, ShowCreationAnim, CursorElement, LimitBarFlagCam, ShowOverdriveBar, Drop1Chance, Drop2Chance, DropGearChance, StealChance, MustBeKilled, IsScanned, IsDistilledPower, IsDistilledMana, IsDistilledSpeed, IsDistilledUnused, IsDistilledAbility, IsShielding, IsBoosting, IsEjected, HasAutoLife, IsCursed, IsDefending, IsGuarding, IsSentinel, IsDoomed, MotionType, DoomInitialCounter, DoomCurrentCounter, DmgDir, DirectionChangeFlag, DirectionChangeEffect, DirectionFixFlag, HitTerminateFlag, DamageTakenHp, DamageTakenMp, DamageTakenCtb, AppearInvisibleFlag, EffectHitNum, AvoidFlag, BlowExistFlag, EscapeFlag, IsHiding, DeathResistance, ZombieResistance, PetrifyResistance, PoisonResistance, PowerBreakResistance, MagicBreakResistance, ArmorBreakResistance, MentalBreakResistance, ConfusionResistance, BerserkResistance, ProvokeResistance, ThreatenChance, SleepResistance, SilenceResistance, DarknessResistance, ShellResistance, ProtectResistance, ReflectResistance, NulWaterResistance, NulFireResistance, NulThunderResistance, NulIceResistance, RegenResistance, HasteResistance, SlowResistance, IsImmuneToScan, IsImmuneToDistillPower, IsImmuneToDistillMana, IsImmuneToDistillSpeed, IsImmuneToDistillUnused, IsImmuneToDistillAbility, IsImmuneToShield, IsImmuneToBoost, IsImmuneToAutoLife, IsImmuneToEject, IsImmuneToCurse, IsImmuneToDefend, IsImmuneToGuard, IsImmuneToSentinel, IsImmuneToDoom, IsVisibleOnFrontlineStats, VisibleCam, VisibleOut, Round, RoundReturn, WinPose, Vigor, FastModelFlag, IsAliveAndNotPetrified, CommandType, EffectTargetFlag, MagicEffectGround, MagicEffectWater, Idle2Prob, AttackMotionType, AttackIncSpeed, AttackDecSpeed, CurrentTurnDelay, AppearCount, MotionNum, HelpMessageId, ScanMessageId, VisibleEff, MotionDisposeFlag, ModelDisposeFlag, DelayResistance, Shadow, Death, DeathStone, CheckPos, WinSe, AttackNum, NearMotion, TalkStat1, TalkStat2, ForceCloseRangeAttackAnim, MotionSpeedNormal, MotionSpeedNormalStart, OwnAttackNear, TalkStat3, CommandSet, RetainsControlWhenProvoked, Provoker, Spellspring, CtbIcon, SoundHitNum, DamageNumPos, Summoner, IsInvincible, IsImmuneToMagic, IsImmuneToPhysical, CanTeachRonsoRage, IsImmuneToZanmato, OverkillThreshold, ReturnMotionType, CamWidth, CamHeight, Height, YojimboCompatibility, YojimboGivenGil, ZanmatoLevel, TurnsTaken, YojimboReaction, AttackNearFrame, MagusSisterMotivation, LimitGaugeAdd, IsNearDeath, HasOverdriveAvailable, HpCheck, MpCheck, HasNulAll, ShellReflect, ProtectReflect, HasteReflect, WeakMotion, IsImmuneToBribe, AttackMotionFrame, MotionTypeReset, MotionTypeAdd, DeathStatus, TargetList, LimitBarPos, IsCenterCharacter, SameTargetCheck, ApRewardNormal, ApRewardOverkill, GilReward, BonusStrength, BonusDefense, BonusMagic, BonusMagicDefense, BonusAgility, BonusLuck, BonusEvasion, BonusAccuracy, UseMp, UseLimit, UseLimitAll, IsMulticasting, Drop1CommonType, Drop1RareType, Drop2CommonType, Drop2RareType, Drop1OverkillCommonType, Drop1OverkillRareType, Drop2OverkillCommonType, Drop2OverkillRareType, Drop1CommonCount, Drop1RareCount, Drop2CommonCount, Drop2RareCount, Drop1OverkillCommonCount, Drop1OverkillRareCount, Drop2OverkillCommonCount, Drop2OverkillRareCount, DeathReturn, LinearMoveReset, BodyHitDirect, IsRecruited, PermanentAutoLife, NeckTargetFlag, VisibleOutOn, RegenDmgFlag, NumPrintElement })
        }
        "MoveProperty" => {
            enum_vars!(enum_name.variant_name : MoveProperty { DmgFormula, DmgType, AffectsHp, AffectsMp, AffectsCtb, Holystrike, Waterstrike, Thunderstrike, Icestrike, Firestrike, TargetType })
        }

        // anim.rs
        "YojimboReaction" => {
            enum_vars!(enum_name.variant_name : YojimboReaction { None, Regular, Nod, Headshake })
        }
        "DeathAnim" => {
            enum_vars!(enum_name.variant_name : DeathAnim { Normal, Linger, FadeOut, Pyreflies, Explosions, BreakDown, BreakDownBevelle })
        }

        // text.rs
        "TextAlign" => enum_vars!(enum_name.variant_name : TextAlign { Left, Right, Center }),

        // menu.rs
        "SubMenu" => {
            enum_vars!(enum_name.variant_name : SubMenu { BlackMagic, WhiteMagic, Skill, Overdrive, Summon, Items, WeaponChange, Escape, SwitchCharacter, Special, ArmorChange, Use, Mix, Gil, Yojimbo })
        }
        "OverdriveCategory" => {
            enum_vars!(enum_name.variant_name : OverdriveCategory { MenuCommand, Tier1, Tier2, Tier3, Tier4 })
        }

        // sphere_grid.rs
        "GridType" => enum_vars!(enum_name.variant_name : GridType { Original, Standard, Expert }),
        "SphereRole" => {
            enum_vars!(enum_name.variant_name : SphereRole { Power, Mana, Speed, Ability, Fortune, Special, Skill, WhiteMagic, BlackMagic, Master, Lv1Key, Lv2Key, Lv3Key, Lv4Key, Luck, Hp, Mp, Return, Friend, Teleport, Warp, Strength, Defense, Magic, MagicDefense, Agility, Evasion, Accuracy, Clear, Attribute })
        }
        "SphereType" => {
            enum_vars!(enum_name.variant_name : SphereType { None, Activation, Modification })
        }

        // audio.rs
        "Sfx" => {
            enum_vars!(enum_name.variant_name : Sfx { StartedNewGame, OpenedChest, PaidGil, PuzzlePrompt, TakeSphereP1, InsertSphere, TakeSphereP2, SolvedPuzzle, GainedLevel, ObtainedTreasure, ObtainedPrimer, SaveSphere, UseCelestialMirror, BoardAirship, ShowInstructions, LightningBolt })
        }
        "Bgm" => {
            enum_vars!(enum_name.variant_name : Bgm { UnwaveringDetermination, SecretManeuvers, BossTheme, TheSummoning, MacalaniaWoods, BattleTheme, SeymoursAmbition, BlitzOff, ThunderPlains, PeopleOfTheNorthPole, TruthRevealed, ToZanarkand, Challenge })
        }

        // script.rs
        "Selector" => {
            enum_vars!(enum_name.variant_name : Selector { AnyAll, Highest, Lowest, Not })
        }
        "Target" => {
            enum_vars!(enum_name.variant_name : Target { None, Active, Target, TargetNow, All, Frontline1, Frontline2, Frontline3, Reserve1, Reserve2, Reserve3, Reserve4, Itself, Frontline, AllEnemies, Predefined, LastAttacker, Input, AllParty2, AllAeons, AllChars, Parent, AllCharsAndAeons, AllPlayer2, AllParty3, Predefined0 })
        }
        "TargetType" => {
            enum_vars!(enum_name.variant_name : TargetType { Single, Group, All, Itself })
        }

        _ => panic!("Enum '{}' not found", enum_name),
    }
}

#[test]
fn get_functions() {
    use crate::script::Func;

    println!("{:?}", Func::get_func(Some("std"), "rand"));
    println!("{:?}", Func::get_func(Some("std"), "__0038"));
    println!("{:?}", Func::get_func(Some("std"), "__0058"));

    println!("{:?}", Func::get_func(Some("battle"), "defineActorSubset"));

    println!("{:?}", Func::get_func(None, "__700B"));
}
