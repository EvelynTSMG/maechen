#[repr(u8)]
pub enum SubMenu {
    BlackMagic = 0x01,
    WhiteMagic = 0x02,
    Skill = 0x03,
    Overdrive = 0x04,
    Summon = 0x05,
    Items = 0x06,
    WeaponChange = 0x07,
    Escape = 0x08,
    SwitchCharacter = 0x0A,
    Special = 0xE,
    ArmorChange = 0xF,
    Use = 0x11,
    Mix = 0x14,
    Gil = 0x15,
    Yojimbo = 0x16,
}

#[repr(u8)]
pub enum OverdriveCategory {
    /// All menu commands (Swordplay, Grand Summon, Bushido, Reels, Fury, Ronso Rage, Mix)
    MenuCommand = 0x1,
    /// All first tier overdrives as well as all Kimahri, Lulu, and Rikku overdrives
    Tier1 = 0x2,
    /// All second tier overdrives (Slice and Dice, Shooting Star, Attack Reels)
    Tier2 = 0x3,
    /// All third tier overdrives (Energy Rain, Banishing Blade, Status Reels)
    Tier3 = 0x4,
    /// All fourth tier overdrives (Blitz Ace, Tornado, Auroch Reels)
    Tier4 = 0x5,
}
