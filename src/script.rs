use seq_macro::seq;

pub enum FuncNamespace {
    Common,
    Math,
    Unk2,
    Unk3,
    SgEvent,
    ChEvent,
    Camera,
    Battle,
    Map,
    Mount,
    UnkA,
    Movie,
    Debug,
    AbiMap,
    UnkE,
    UnkF,
}

#[derive(Clone, Debug)]
pub enum VarType {
    Void,
    /// Used only by the compiler
    Bool,
    Int,
    Float,
    /// Used only by the compiler
    ///
    /// Enum(id)
    Enum(&'static str),
    Unk,
}

#[repr(u8)]
pub enum Selector {
    AnyAll = 0x0,
    Highest = 0x1,
    Lowest = 0x2,
    Not = 0x3,
}

#[repr(u16)]
pub enum Target {
    /// Internal name: `CHR_NOP`
    None = 0xFFFF, //TODO: Figure this out. Where is this used? Provide example or "Unused"
    /// Internal name: `CHR_ACTIVE`
    Active = 0xFFFE,
    /// Internal name: `CHR_TARGET`
    Target = 0xFFFD,
    //? Adjacent?
    /// Internal name: `CHR_TARGET_NOW`
    TargetNow = 0xFFFC, //TODO: Figure this out. Where is this used? Provide example or "Unused"
    /// Internal name: `CHR_ALL`
    All = 0xFFFB,
    /// Internal name: `CHR_PARTY1`
    Frontline1 = 0xFFFA,
    /// Internal name: `CHR_PARTY2`
    Frontline2 = 0xFFF9,
    /// Internal name: `CHR_PARTY3`
    Frontline3 = 0xFFF8,
    /// Internal name: `CHR_PARTY4`
    ///
    /// Used mainly to target reserve character ctb to make sure enemy moves first
    Reserve1 = 0xFFF7,
    /// Internal name: `CHR_PARTY5`
    ///
    /// Used mainly to target reserve character ctb to make sure enemy moves first
    Reserve2 = 0xFFF6,
    /// Internal name: `CHR_PARTY6`
    ///
    /// Used mainly to target reserve character ctb to make sure enemy moves first
    Reserve3 = 0xFFF5,
    /// Internal name: `CHR_PARTY7`
    ///
    /// Used mainly to target reserve character ctb to make sure enemy moves first
    Reserve4 = 0xFFF4,
    /// Internal name: `CHR_OWN`
    Itself = 0xFFF3,
    /// Internal name: `CHR_ALL_PLY`
    Frontline = 0xFFF2,
    /// Internal name: `CHR_ALL_MON`
    AllEnemies = 0xFFF1,
    /// Internal name: `CHR_OWN_TARGET`
    ///
    /// Targets the predefined group set through a script
    Predefined = 0xFFF0,
    /// Internal name: `CHR_REACTION`
    ///
    /// Targets the last attacker, used in counter-attacks
    LastAttacker = 0xFFEF,
    /// Internal name: `CHR_INPUT`
    Input = 0xFFEE, //TODO: Figure this out
    /// Internal name: `CHR_ALL_PLY2`
    AllParty2 = 0xFFED, //TODO: Figure this out
    /// Internal name: `CHR_ALL_SUMMON`
    ///
    /// Used mainly to check if any aeons are on the frontline with countActorsIn
    AllAeons = 0xFFEC,
    /// Internal name: `CHR_ALL2`
    AllChars = 0xFFEB, //TODO: Where is this used? Provide example or "Unused"
    //? Mortibody?
    /// Internal name: `CHR_PARENT`
    Parent = 0xFFEA, //TODO: Test guess
    /// Internal name: `CHR_ALL_PLAYER`
    AllCharsAndAeons = 0xFFE9, //TODO: Where is this used? Provide example or "Unused"
    /// Internal name: `CHR_ALL_PLAYER2`
    AllPlayer2 = 0xFFE8, //TODO: Figure this out
    /// Internal name: `CHR_ALL_PLY3`
    AllParty3 = 0xFFE7, //TODO: Figure this out
    //? First from filter?
    /// Internal name: `CHR_OWN_TARGET0`
    Predefined0 = 0xFFE6, //TODO: Figure this out
}

#[repr(u8)]
pub enum TargetType {
    /// Internal name: `target_type_single`
    Single = 0x0,
    /// Internal name: `target_type_group`
    Group = 0x1,
    /// Internal name: `target_type_all`
    All = 0x2,
    /// Internal name: `target_type_own`
    Itself = 0x3,
}

#[derive(Clone, Debug)]
pub struct FuncArg {
    name: &'static str,
    typ: VarType,
}

#[derive(Clone, Debug)]
pub struct Func {
    pub addr: u16,
    pub name: &'static str,
    pub args: &'static [FuncArg],
    pub return_type: VarType,
}

macro_rules! func {
    (
        fn $fname:ident $($__0:ident)? (
            $(
                $arg_name:ident:
                ([$ArgType:ident $( $__1:ident )?] $($EnumId:ident)?)
            )*
        ) -> $Ret:ident $($__2:ident)? @ $addr:expr;
    ) => {
        Func {
            name: stringify!($fname),
            args: &[
                $( // For each repetition (args):
                    FuncArg {
                        name: stringify!($arg_name),
                        typ: VarType::$ArgType$((stringify!($EnumId)))?, // ArgType, or Unk if missing
                    },
                )*
            ],
            return_type: VarType::$Ret, // Ret or Void if missing
            addr: $addr,
        }
    }
}

macro_rules! funcs {
    (
        $( // Repetition of:
            fn $($fname:ident)? ( // matches `fn func_name (`
                $( // Repetition of:
                    $arg_name:ident$(: $ArgType:ident$(($EnumId:ident))?)? // matches `arg_name: arg_type(?:(enum_name))?`
                ),* // zero or more
                $(,)? // comma separated
            ) $(-> $Ret:ident )?@ $addr:literal; // matches `) -> return_type @ address;`
        )+ // one or more
    ) => {
        [
            $( // For each repetition (funcs):
                seq! {
                    address in $addr..=$addr {
                        func! {
                            fn $($fname)? __~address (
                                $(
                                    $arg_name: ([$($ArgType)? Unk] $($($EnumId)?)?)
                                )*
                            ) -> $($Ret)? Void @ $addr;
                        }
                    }
                },
            )*
        ]
    };
}

#[allow(nonstandard_style)]
impl Func {
    pub fn get_func(namespace: Option<&str>, func_name: &str) -> Vec<Func> {
        if let Some(namespace) = namespace {
            let funcs: &[Func];
            match namespace {
                "std" => funcs = &Func::std,
                "math" => funcs = &Func::math,
                "unk2" => funcs = &Func::unk2,
                "unk3" => funcs = &Func::unk3,
                "sgEvent" => funcs = &Func::sgEvent,
                "chEvent" => funcs = &Func::chEvent,
                "cam" => funcs = &Func::cam,
                "battle" => funcs = &Func::battle,
                "map" => funcs = &Func::map,
                "mount" => funcs = &Func::mount,
                "unkA" => funcs = &Func::unkA,
                "movie" => funcs = &Func::movie,
                "debug" => funcs = &Func::debug,
                "abiMap" => funcs = &Func::abiMap,
                "unkE" => funcs = &Func::unkE,
                "unkF" => funcs = &Func::unkF,
                _ => panic!("Namespace '{}' not found", namespace),
            }

            if funcs.is_empty() {
                panic!("There are no known functions for namespace '{}'", namespace);
            }

            let mut funcs = funcs.to_vec();
            funcs.retain(|f| f.name == func_name);

            if funcs.is_empty() {
                panic!("Function '{}::{}' not found", namespace, func_name)
            }

            funcs
        } else {
            let mut funcs = Func::std.to_vec();
            funcs.extend(Func::math);
            funcs.extend(Func::unk2);
            funcs.extend(Func::unk3);
            funcs.extend(Func::sgEvent);
            funcs.extend(Func::chEvent);
            funcs.extend(Func::cam);
            funcs.extend(Func::battle);
            funcs.extend(Func::map);
            funcs.extend(Func::mount);
            funcs.extend(Func::unkA);
            funcs.extend(Func::movie);
            funcs.extend(Func::debug);
            funcs.extend(Func::abiMap);
            funcs.extend(Func::unkE);
            funcs.extend(Func::unkF);

            funcs.retain(|f| format!("__{:0>4X}", f.addr) == func_name);

            funcs
        }
    }

    pub const std: [Func; 443] = funcs! {
        fn wait(frames: Int) @ 0x0000;
        fn loadModel(model) @ 0x0001;
        fn (unk1, unk2, unk3) @ 0x0002;
        fn attachToLevelPart(part_idx: Int) @ 0x0003;
        fn attachToMapGroup(group_idx: Int) @ 0x0004;
        fn applyTransform() @ 0x0005;
        fn (unk1, unk2, unk3) @ 0x0006;
        fn (unk) @ 0x0007;
        fn () @ 0x0010;
        fn (unk1, unk2) @ 0x0011;
        fn (unk1, unk2) @ 0x0012;
        fn setPosition(x: Float, y: Float, z: Float) @ 0x0013;
        fn (unk1, unk2, unk3) @ 0x0015;
        fn setMotionSpeed(speed: Float) @ 0x0016;
        fn (unk) @ 0x0017;
        fn (unk1, unk2, unk3) @ 0x0018;
        fn startRotation(active_bits: Int, flags: Int, target_object_idx: Int) @ 0x0019;
        fn () @ 0x001A;
        fn () @ 0x001B;
        fn (unk1, unk2) @ 0x001C;
        fn setRotationTiming(cur_time: Float, duration: Float) @ 0x001D;
        fn () -> Float @ 0x001F;
        fn () @ 0x0020;
        fn (unk) @ 0x0021;
        fn setVelocityYaw(angle: Float) @ 0x0023;
        fn setVelocityPitch(angle: Float) @ 0x0024;
        fn (unk) @ 0x0025;
        fn setRotationTarget1(angle: Float) @ 0x0028;
        fn setRotationTarget2(angle: Float) @ 0x0029;
        fn setRotationTarget3(angle: Float) @ 0x002A;
        fn (unk) @ 0x002B;
        fn (unk) @ 0x002C;
        fn (unk) @ 0x002D;
        fn (unk) @ 0x002E;
        fn (unk) @ 0x002F;
        fn (unk) @ 0x0030;
        fn (unk) @ 0x0033;
        fn (unk) @ 0x0034;
        fn (unk) @ 0x0035;
        fn (unk) @ 0x0036;
        fn (unk) @ 0x0037;
        fn (unk1: Float) @ 0x0038;
        fn (unk1: Float) @ 0x0039;
        fn (unk1: Float) @ 0x003A;
        fn () @ 0x003D;
        fn () @ 0x003F;
        fn linkFieldToBattleActor(actor: Int) @ 0x0042;
        fn () @ 0x0043;
        fn () @ 0x0044;
        fn () @ 0x0046;
        fn () @ 0x0047;
        fn (unk) @ 0x004C;
        fn controllerButtonPressed1(button: Int) -> Bool @ 0x004D;
        fn (unk) @ 0x0050;
        fn controllerButtonPressed2(button: Int) -> Bool @ 0x0051;
        fn (unk1, unk2, unk3, unk4, unk5, unk6) @ 0x0054;
        fn (unk) @ 0x0055;
        fn (unk) @ 0x0056;
        fn (unk1, unk2, unk3) @ 0x0057;
        fn (unk1, unk2) @ 0x0058;
        fn (unk1, unk2, unk3) @ 0x0059;
        fn (unk) @ 0x005A;
        fn (unk) @ 0x005C;
        fn enablePlayerControl() @ 0x005D;
        fn disablePlayerControl() @ 0x005E;
        fn halt() @ 0x005F;
        fn (unk) @ 0x0060;
        fn (unk) @ 0x0061;
        fn (unk) @ 0x0062;
        fn (unk) @ 0x0063;
        fn displayFieldString(box_idx: Int, string_id: Int) @ 0x0064;
        fn positionText(box_idx: Int, x: Int, y: Int, text_align: Int) @ 0x0065;
        fn (unk1, unk2) @ 0x0066;
        fn (unk1, unk2, unk3) @ 0x0069;
        fn (unk1, unk2) @ 0x006A;
        fn (unk) @ 0x006B;
        fn (unk) @ 0x006C;
        fn (unk) @ 0x006D;
        fn (unk) @ 0x006E;
        fn setAllRotationScale1(scale: Float) @ 0x006F;
        fn setAllRotationScale2(scale: Float) @ 0x0070;
        fn setAllRotationScale3(scale: Float) @ 0x0071;
        fn (unk) @ 0x0074;
        fn () @ 0x0076;
        fn stopObjectMotion(idx: Int) @ 0x0077;
        fn (unk) @ 0x0078;
        fn (unk1, unk2) @ 0x007A;
        fn (unk) @ 0x007C;
        fn (unk) @ 0x007D;
        fn (unk) @ 0x007F;
        fn () @ 0x0080;
        fn () @ 0x0081;
        fn () @ 0x0082;
        fn () @ 0x0083;
        fn (unk1, unk2) @ 0x0084;
        fn (unk) @ 0x0085;
        fn () @ 0x0086;
        fn () @ 0x0087;
        fn () @ 0x0088;
        fn () @ 0x0089;
        fn () @ 0x008B;
        fn (unk) @ 0x008D;
        fn (unk) @ 0x008E;
        fn (unk) @ 0x008F;
        fn () @ 0x0090;
        fn () @ 0x0091;
        fn (unk) @ 0x0092;
        fn (unk) @ 0x0093;
        fn setGravity(gravity: Int) @ 0x0094;
        fn (unk: Float) @ 0x0095;
        fn (unk) @ 0x0096;
        fn (unk) @ 0x0097;
        fn (unk1, unk2) @ 0x0098;
        fn (unk) @ 0x009A;
        fn (unk1, unk2) @ 0x009B;
        fn (unk1, unk2) @ 0x009D;
        fn prepareStringIntVar(box_idx: Int, var_idx: Int, unk3, value: Int) @ 0x009E;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x00A2;
        fn (unk1, unk2, unk3, unk4) @ 0x00A3;
        fn (unk1, unk2) @ 0x00A4;
        fn (unk1, unk2) @ 0x00A5;
        fn rand(max: Int) -> Int @ 0x00A6;
        fn (unk) @ 0x00A7;
        fn (unk) @ 0x00A8;
        fn rand() -> Int @ 0x00A9;
        fn (unk) @ 0x00AA;
        fn (unk1, unk2) @ 0x00AB;
        fn (unk) @ 0x00AC;
        fn (unk) @ 0x00B1;
        fn (unk) @ 0x00B2;
        fn (unk) @ 0x00B3;
        fn (unk) @ 0x00B4;
        fn () @ 0x00BA;
        fn (unk) @ 0x00BB;
        fn (unk) @ 0x00BC;
        fn (unk) @ 0x00BD;
        fn (unk) @ 0x00BE;
        fn (unk) @ 0x00BF;
        fn (unk) @ 0x00C0;
        fn (unk) @ 0x00C1;
        fn (unk) @ 0x00C2;
        fn () @ 0x00C4;
        fn registerTurnMotions(motion1, motion2, motion3, motion4) @ 0x00C5;
        fn () @ 0x00C6;
        fn (unk) @ 0x00C7;
        fn (unk1, unk2, unk3) @  0x00C8;
        fn (unk) @ 0x00C9;
        fn addPartyMember(character: Int) @ 0x00CA;
        fn removePartyMember(character: Int) @ 0x00CB;
        fn (unk) @ 0x00CC;
        fn (unk) @ 0x00CE;
        fn (unk) @ 0x00CF;
        fn () @ 0x00D0;
        fn () @ 0x00D1;
        fn () @ 0x00D2;
        fn playFieldVoice(voice_idx: Int) @ 0x00D5;
        fn () @ 0x00D6;
        fn () @ 0x00D7;
        fn () @ 0x00D8;
        fn () @ 0x00D9;
        fn () @ 0x00DA;
        fn loadMoveAnim(move_anim) @ 0x00DB;
        fn playLoadedMoveAnim() @ 0x00DC;
        fn () @ 0x00DE;
        fn playSfx(sfx: Enum(Sfx)) @ 0x00DF;
        fn (unk) @ 0x00E0;
        fn () @ 0x00E1;
        fn (unk) @ 0x00E2;
        fn (unk) @ 0x00E3;
        fn (unk) @ 0x00E4;
        fn (unk) @ 0x00E6;
        fn putPartymemberInSlot(slot: Int, character: Enum(Character)) @ 0x00E7;
        fn (unk1, unk2, unk3) @ 0x00E8;
        fn (unk) @ 0x00E9;
        fn () @ 0x00EB;
        fn (unk) @ 0x00EC;
        fn () @ 0x00ED;
        fn (unk) @ 0x00EE;
        fn (unk) @ 0x00EF;
        fn (unk) @ 0x00F2;
        fn (unk) @ 0x00F3;
        fn yawToPoint(x: Float, y: Float, z: Float) -> Float @ 0x00F4;
        fn (unk1, unk2, unk3) @ 0x00F5;
        fn (unk1, unk2) @ 0x00F6;
        fn (unk1, unk2, unk3) @ 0x00F8;
        fn () @ 0x00F9;
        fn (unk) @ 0x00FA;
        fn (unk) @ 0x00FB;
        fn (unk1, unk2, unk3) @ 0x00FD;
        fn (unk1, unk2) @ 0x00FE;
        fn (unk1, unk2, unk3) @ 0x0100;
        fn setBgmToLoad(bgm: Enum(Bgm)) @ 0x0102;
        fn unloadBgm(bgm: Enum(Bgm)) @ 0x0103;
        fn playBgm(bgm: Enum(Bgm)) @ 0x0104;
        fn loadBgm() @ 0x0105;
        fn (unk1, unk2, unk3) @ 0x0106;
        fn (unk1, unk2) @ 0x0107;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x0108;
        fn (unk) @ 0x0109;
        fn (unk) @ 0x010A;
        fn (unk1, unk2) @ 0x010B;
        fn (unk1, unk2) @ 0x010C;
        fn collectPrimer(primer_idx: Int) @ 0x010D;
        fn getCollectedPrimers() -> Int @ 0x010E;
        fn (unk1, unk2, unk3, unk4) @ 0x010F;
        fn (unk) @ 0x0110;
        fn () @ 0x0111;
        fn () @ 0x0112;
        fn (unk1, unk2) @ 0x0114;
        fn (unk) @ 0x0115;
        fn (unk) @ 0x0116;
        fn (unk) @ 0x0117;
        fn (unk) @ 0x0119;
        fn (unk1, unk2, unk3) @ 0x011A;
        fn (unk1, unk2) @ 0x011B;
        fn (unk) @ 0x011C;
        fn (unk1, unk2) @ 0x011D;
        fn (unk1, unk2, unk3) @ 0x011F;
        fn (unk1, unk2, unk3) @ 0x0120;
        fn getStickTilt(axis: Enum(Axis)) -> Int @ 0x0121;
        fn (unk) @ 0x0122;
        fn (unk1, unk2, unk3) @ 0x0126;
        fn (unk) @ 0x0127;
        fn (unk1: Float, unk2: Int) -> Int @ 0x0128;
        fn (unk1, unk2, unk3, unk4) @ 0x0129;
        fn (unk1, unk2) @ 0x012A;
        fn (unk) @ 0x012B;
        fn (unk, unk) -> Int @ 0x012C;
        fn getAffection(chr_id: Int) -> Int @ 0x012D;
        fn addAffection(chr_id: Int, change: Int) -> Int @ 0x012E;
        fn reduceAffection(chr_id: Int, change: Int) -> Int @ 0x012F;
        fn setAffection(chr_id: Int, amount: Int) -> Int @ 0x0130;
        fn (unk1, unk2) @ 0x0132;
        fn (unk1, unk2) @ 0x0133;
        fn (model: Enum(Model)) @ 0x0134;
        fn getTotalGil() -> Int @ 0x0135;
        fn addGil(amount: Int) -> Bool @ 0x0136;
        fn payGil(amount: Int) -> Bool @ 0x0137;
        fn setGil(amount: Int) -> Int @ 0x0138;
        fn requestNumInput(unk1, unk2, unk3, unk4, unk5, unk6, unk7, align: Enum(TextAlignment)) -> Int @ 0x0139;
        fn displayFieldChoice(box_idx: Int, unk1, unk2, unk3, unk4, unk5, align: Enum(TextAlignment)) -> Int @ 0x013B;
        fn (unk) @ 0x013D;
        fn (unk) @ 0x013E;
        fn (unk1, unk2) @ 0x013F;
        fn (unk1, unk2, unk3, unk4, unk5, unk6) @ 0x0140;
        fn (unk1, unk2, unk3) @ 0x0141;
        fn (unk1, unk2) @ 0x0142;
        fn (unk) @ 0x0143;
        fn (unk) @ 0x0144;
        fn (unk1, unk2) @ 0x0145;
        fn (unk) @ 0x0146;
        fn (unk1, unk2) @ 0x0148;
        fn (unk) @ 0x0149;
        fn (unk1, unk2, unk3) @ 0x014A;
        fn () @ 0x014D;
        fn (unk1, unk2, unk3) @ 0x014E;
        fn (unk1, unk2, unk3) @ 0x014F;
        fn (unk) @ 0x0151;
        fn () @ 0x0154;
        fn (unk) @ 0x0155;
        fn () @ 0x0156;
        fn (unk) @ 0x0157;
        fn (unk) @ 0x0158;
        fn (unk1, unk2) @ 0x0159;
        fn obtainTreasure(textbox_idx: Int, treasure) @ 0x015B;
        fn (unk) @ 0x015D;
        fn (unk) @ 0x015E;
        fn (unk) @ 0x015F;
        fn hasKeyItem(item_id: Int) -> Bool @ 0x0160;
        fn (unk) @ 0x0161;
        fn (unk) @ 0x0166;
        fn () @ 0x0167;
        fn (unk1, unk2) @ 0x016A;
        fn (unk1, unk2, unk3) @ 0x016B;
        fn () @ 0x016C;
        fn (bgm: Enum(Bgm)) @ 0x016D;
        fn (unk1, unk2) @ 0x016E;
        fn (unk) @ 0x016F;
        fn restoreCharHp(char: Enum(Character)) @ 0x0171;
        fn restoreCharMp(char: Enum(Character)) @ 0x0172;
        fn () @ 0x0174;
        fn (unk) @ 0x0177;
        fn (unk) @ 0x0179;
        fn (unk) @ 0x017A;
        fn (unk) @ 0x017B;
        fn (unk) @ 0x017C;
        fn playBgmInclBattle(bgm: Enum(Bgm), unk) @ 0x017E;
        fn (unk) @ 0x017F;
        fn (unk1, unk2) @ 0x0180;
        fn () @ 0x0181;
        fn (unk) @ 0x0184;
        fn (unk1, unk2) @ 0x0185;
        fn (unk) @ 0x0188;
        fn (unk) @ 0x0189;
        fn (unk) @ 0x018A;
        fn (unk) @ 0x018B;
        fn (unk1, unk2) @ 0x018F;
        fn (unk) @ 0x0192;
        fn (unk) @ 0x0193;
        fn (unk) @ 0x0194;
        fn () @ 0x0195;
        fn (unk) @ 0x0196;
        fn (unk) @ 0x0197;
        fn enableVisEff(level_part_idx: Int, effect_type, run_once: Bool) @ 0x0198;
        fn enableOwnVisEff(effect_type, run_once: Bool) @ 0x0199;
        fn () @ 0x019C;
        fn (unk) @ 0x019D;
        fn () @ 0x019E;
        fn (unk) @ 0x019F;
        fn (unk1, unk2, unk3) @ 0x01A0;
        fn (unk1, unk2) @ 0x01A4;
        fn (unk1, unk2) @ 0x01A5;
        fn () @ 0x01A6;
        fn obtainTreasureSilent(treasure) @ 0x01A7;
        fn (unk) @ 0x01A8;
        fn getEncounterCount() -> Int @ 0x01AB;
        fn (unk) @ 0x01AC;
        fn (unk) @ 0x01AD;
        fn (unk1, unk2) @ 0x01AF;
        fn (unk1, unk2) @ 0x01B0;
        fn () @ 0x01B1;
        fn getItemCount(item_id: Int) -> Int @ 0x01B2;
        fn () @ 0x01B5;
        fn () @ 0x01B6;
        fn (unk) @ 0x01B7;
        fn () @ 0x01B8;
        fn (unk1, unk2) @ 0x01BA;
        fn () @ 0x01BB;
        fn (unk) @ 0x01BC;
        fn (unk) @ 0x01BD;
        fn () @ 0x01BF;
        fn (unk1, unk2) @ 0x01C0;
        fn () @ 0x01C2;
        fn (unk) @ 0x01C3;
        fn (unk) @ 0x01C4;
        fn (unk1, unk2) @ 0x01C5;
        fn () @ 0x01C6;
        fn disableOwnVisEff(effect_type) @ 0x01C8;
        fn disableVisEff(level_part_idx: Int, effect_type) @ 0x01C9;
        fn () @ 0x01CA;
        fn () @ 0x01CC;
        fn () @ 0x01CE;
        fn () @ 0x01CF;
        fn () @ 0x01D0;
        fn (unk) @ 0x01D1;
        fn enteredShipPassEquals(pass) -> Bool @ 0x01D2;
        fn (unk1, unk2) @ 0x01D4;
        fn () @ 0x01D9;
        fn (unk1, unk2, unk3, unk4) @ 0x01DA;
        fn () @ 0x01DC;
        fn () @ 0x01DE;
        fn (unk) @ 0x01E0;
        fn () @ 0x01E2;
        fn () @ 0x01E3;
        fn (unk1, unk2, unk3, unk4) @ 0x01E4;
        fn (unk1, unk2, unk3) @ 0x01E5;
        fn (unk1, unk2, unk3) @ 0x01E6;
        fn (unk1, unk2) @ 0x01E7;
        fn (unk1, unk2) @ 0x01E9;
        fn (unk) @ 0x01EB;
        fn () @ 0x01EE;
        fn (unk) @ 0x01F0;
        fn () @ 0x01F2;
        fn (unk1, unk2, unk3) @ 0x01F3;
        fn (unk1, unk2) @ 0x01F4;
        fn (unk1, unk2) @ 0x01F8;
        fn (unk) @ 0x01F9;
        fn () @ 0x01FA;
        fn () @ 0x01FB;
        fn teachAbilitySilent(char: Enum(Character), ability) @ 0x01FC;
        fn (unk) @ 0x0200;
        fn () @ 0x0201;
        fn (unk) @ 0x0202;
        fn (model) @ 0x0203;
        fn (unk) @ 0x0204;
        fn () @ 0x0205;
        fn () @ 0x0206;
        fn () @ 0x0207;
        fn () @ 0x0209;
        fn () @ 0x020A;
        fn () @ 0x020B;
        fn (unk) @ 0x020D;
        fn (unk) @ 0x020F;
        fn unlockMonsterArenaEntry(entry_id: Int) @ 0x0210;
        fn lockMonsterArenaEntry(entry_id: Int) @ 0x0211;
        fn (unk1, unk2) @ 0x0212;
        fn (unk) @ 0x0213;
        fn grantCelestialUpgrade(char: Enum(Character), level: Enum(CelestialLevel)) @ 0x0215;
        fn teachAbility(box_idx: Int, char: Enum(Character), ability) @ 0x0216;
        fn () @ 0x0217;
        fn (unk1, unk2) @ 0x021A;
        fn (unk1, unk2) @ 0x021B;
        fn () @ 0x021D;
        fn () @ 0x021E;
        fn (unk) @ 0x0220;
        fn (unk) @ 0x0221;
        fn setShopRate(percentage: Int) @ 0x0225;
        fn (unk) @ 0x0226;
        fn () @ 0x0229;
        fn (unk) @ 0x022D;
        fn (unk1, unk2, unk3, unk4) @ 0x022E;
        fn (unk1, unk2) @ 0x022F;
        fn (unk) @ 0x0231;
        fn () @ 0x0234;
        fn () @ 0x0235;
        fn (unk) @ 0x0236;
        fn (unk1, unk2, unk3, unk4) @ 0x0237;
        fn (unk) @ 0x0239;
        fn (unk) @ 0x023A;
        fn (unk) @ 0x023B;
        fn (unk1, unk2) @ 0x023C;
        fn () @ 0x023D;
        fn (unk) @ 0x023F;
        fn (unk1, unk2) @ 0x0240;
        fn (unk) @ 0x0241;
        fn () @ 0x0242;
        fn () @ 0x0243;
        fn () @ 0x0244;
        fn () @ 0x0245;
        fn (unk) @ 0x024A;
        fn (unk1, unk2) @ 0x024B;
        fn () @ 0x024C;
        fn (unk) @ 0x024D;
        fn (unk) @ 0x0251;
        fn () @ 0x0253;
        fn setSphereGrid(sphere_grid: Enum(GridType)) @ 0x0254;
        fn () @ 0x0255;
        fn (unk) @ 0x0256;
        fn (unk) @ 0x0257;
        fn (unk1, unk2) @ 0x0259;
        fn unlockAchievement(ach_id) @ 0x025B;
        fn () @ 0x025C;
        fn (unk) @ 0x025D;
        fn (unk) @ 0x025E;
        fn (unk1, unk2) @ 0x025F;
        fn (unk) @ 0x0260;
        fn (unk) @ 0x0261;
        fn () @ 0x0262;
        fn () @ 0x0263;
        fn (unk) @ 0x0264;
        fn () @ 0x0265;
        fn () @ 0x0266;
        fn (unk1, unk2, unk3, unk4, unk5, unk6, unk7) @ 0x0267;
    };

    pub const math: [Func; 30] = funcs! {
        fn (unk1, unk2, unk3, unk4) @ 0x1000;
        fn sin(x: Float) -> Float @ 0x1001;
        fn cos(x: Float) -> Float @ 0x1002;
        fn tan(x: Float) -> Float @ 0x1003;
        fn atan(x: Float) -> Float @ 0x1004;
        fn (unk1, unk2) @ 0x1005;
        fn sqrt(x: Float) -> Float @ 0x1006;
        fn inverse_sqrt(x: Float) -> Float @ 0x1007;
        fn (x: Int) -> Int @ 0x1008;
        fn (x: Int) -> Int @ 0x1009;
        fn (x: Int) -> Int @ 0x100A;
        fn (x: Int) -> Int @ 0x100B;
        fn (x: Float) -> Int @ 0x100C;
        fn (x: Float) -> Int @ 0x100D;
        fn (x: Float) -> Int @ 0x100E;
        fn (x: Int) -> Float @ 0x100F;
        fn (x: Int) -> Float @ 0x1010;
        fn getAngleTo(orig_x, orig_y, ref_x, ref_y) -> Float @ 0x1011;
        fn (unk1: Int, unk2: Int, unk3: Float, unk4: Float, unk5: Float, unk6: Float, unk7: Float, unk8: Float) -> Float @ 0x1012;
        fn modPi(x: Float) -> Float @ 0x1013;
        fn (unk1: Float, unk2: Float, unk3: Float) -> Float @ 0x1014;
        fn (orig_x: Float, orig_y: Float, ref_x: Float, ref_y: Float, unk5: Float, Float, unk6: Float) -> Int @ 0x1015;
        fn (unk1: Float, unk2: Float, unk3: Float, unk4: Float, unk5: Float, unk6: Float, unk7: Float) -> Int @ 0x1016;
        fn (unk1: Int, unk2: Int, unk3: Int, unk4: Float) -> Float @ 0x1017;
        fn (unk1: Int, unk2: Float, unk3: Float, unk4: Float) -> Float @ 0x1018;
        fn abs(x: Float) -> Float @ 0x1019;
        fn (unk1: Float, unk2: Float, unk3: Float, unk4: Float) -> Float @ 0x101A;
        fn (unk1: Float, unk2: Float, unk3: Float, unk4: Float, unk5: Float, unk6: Float) -> Float @ 0x101B;
        fn (unk1: Int, unk2: Int) -> Int @ 0x101C;
        fn (unk1: Int, unk2: Int) -> Int @ 0x101D;
    };

    pub const unk2: [Func; 0] = [];
    pub const unk3: [Func; 0] = [];

    pub const sgEvent: [Func; 44] = funcs! {
        fn (unk) @ 0x4001;
        fn fadeInFromColor(length: Int, r: Int, g: Int, b: Int) @ 0x4003;
        fn fadeInFromBlack(length: Int) @ 0x4004;
        fn fadeOutToBlack(length: Int) @ 0x4005;
        fn fadeInFromWhite(length: Int) @ 0x4006;
        fn fadeOutToWhite(length: Int) @ 0x4007;
        fn setScreenTint(r: Int, g: Int, b: Int, a: Int) @ 0x4008;
        fn dimScreen(r: Int, g: Int, b: Int, factor: Int) @ 0x4009;
        fn setMotionBlur(alpha: Int) @ 0x400A;
        fn disableFade() @ 0x400B;
        fn disableScreenTint() @ 0x400C;
        fn waitForFade() @ 0x400D;
        fn (unk) @ 0x400E;
        fn (unk) @ 0x400F;
        fn (unk1, unk2, unk3, unk4, unk5, unk6) @ 0x4013;
        fn (unk) @ 0x4014;
        fn (unk1, unk2) @ 0x4015;
        fn (unk1, unk2) @ 0x4016;
        fn (unk1, unk2) @ 0x4017;
        fn (unk) @ 0x4019;
        fn camXFade(frames: Int, alpha: Int) @ 0x401A;
        fn (unk) @ 0x401B;
        fn (unk) @ 0x401C;
        fn showModularMenu(menu) @ 0x401D;
        fn () @ 0x401F;
        fn () @ 0x4020;
        fn (unk) @ 0x4022;
        fn (unk) @ 0x4023;
        fn () @ 0x4024;
        fn (unk1, unk2, unk3, unk4, unk5, unk6, unk7, unk8, unk9) @ 0x4025;
        fn (unk1, unk2) @ 0x402A;
        fn (unk) @ 0x4034;
        fn (unk1, unk2, unk3) @ 0x4036;
        fn (unk1, unk2, unk3, unk4) @ 0x4039;
        fn (unk) @ 0x403A;
        fn (unk1, unk2) @ 0x403B;
        fn (unk) @ 0x403C;
        fn (unk1, unk2) @ 0x403E;
        fn () @ 0x403F;
        fn (unk) @ 0x4040;
        fn (unk1, unk2) @ 0x4043;
        fn (unk1, unk2, unk3) @ 0x4044;
        fn (unk) @ 0x4045;
        fn (unk) @ 0x4046;
    };

    pub const chEvent: [Func; 112] = funcs! {
        fn (unk) @ 0x5000;
        fn (unk) @ 0x5001;
        fn () @ 0x5003;
        fn () @ 0x5005;
        fn (unk) @ 0x5006;
        fn (unk1, unk2, unk3, unk4) @ 0x5007;
        fn (unk) @ 0x5008;
        fn (unk) @ 0x5009;
        fn (unk) @ 0x500B;
        fn (unk) @ 0x500C;
        fn (unk) @ 0x500D;
        fn (unk) @ 0x500E;
        fn (unk) @ 0x500F;
        fn (model, unk) @ 0x5010;
        fn (unk) @ 0x5013;
        fn (unk1, unk2) @ 0x5014;
        fn (unk1, unk2, unk3, unk4) @ 0x5015;
        fn (unk1, unk2, unk3) @ 0x5016;
        fn (unk) @ 0x5017;
        fn (unk) @ 0x5018;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x5019;
        fn (motion, unk1, unk2, unk3, unk4) @ 0x501A;
        fn playCharAnim(motion) @ 0x501B;
        fn () @ 0x501E;
        fn () @ 0x501F;
        fn () @ 0x5020;
        fn (unk) @ 0x5021;
        fn (unk) @ 0x5022;
        fn (unk, motion) @ 0x5025;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x5026;
        fn (unk) @ 0x5028;
        fn (unk) @ 0x5029;
        fn (unk) @ 0x502A;
        fn (unk1, unk2) @ 0x502C;
        fn (unk1, unk2) @ 0x502D;
        fn () @ 0x5032;
        fn (unk1, unk2) @ 0x5033;
        fn (unk) @ 0x5034;
        fn () @ 0x5035;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x5036;
        fn (unk1, unk2, unk3, unk4) @ 0x5037;
        fn (unk) @ 0x5039;
        fn (unk) @ 0x503A;
        fn (unk1, unk2) @ 0x503B;
        fn (unk) @ 0x503C;
        fn (unk) @ 0x503D;
        fn (unk1, unk2) @ 0x503E;
        fn (unk) @ 0x503F;
        fn (motion) @ 0x5040;
        fn (motion, unk1, unk2, unk3, unk4) @ 0x5042;
        fn (unk1, unk2, unk3, unk4, unk5, unk6) @ 0x5044;
        fn (unk1, unk2, unk3, unk4, unk5, unk6) @ 0x5046;
        fn (unk1, unk2) @ 0x5047;
        fn (unk1, unk2) @ 0x5048;
        fn (unk) @ 0x5049;
        fn (unk) @ 0x504A;
        fn (unk) @ 0x504B;
        fn (unk1, unk2) @ 0x504C;
        fn (unk) @ 0x504D;
        fn (model) @ 0x504E;
        fn (model) @ 0x504F;
        fn (unk1, unk2) @ 0x5050;
        fn () @ 0x5051;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x5052;
        fn (unk1, unk2) @ 0x5054;
        fn (unk) @ 0x5056;
        fn (unk1, unk2) @ 0x5057;
        fn (unk) @ 0x5058;
        fn (unk1, unk2) @ 0x505A;
        fn (unk) @ 0x505C;
        fn (motion) @ 0x505D;
        fn () @ 0x505E;
        fn (unk) @ 0x505F;
        fn (unk) @ 0x5060;
        fn (model, unk) @ 0x5061;
        fn (model, unk) @ 0x5063;
        fn (unk) @ 0x5065;
        fn (unk) @ 0x5066;
        fn (unk) @ 0x5067;
        fn () @ 0x5068;
        fn (unk) @ 0x5069;
        fn (unk) @ 0x506A;
        fn (unk1, unk2) @ 0x506D;
        fn (unk) @ 0x506E;
        fn (unk) @ 0x506F;
        fn (unk) @ 0x5071;
        fn () @ 0x5072;
        fn (unk) @ 0x5073;
        fn (unk) @ 0x5075;
        fn (unk) @ 0x5076;
        fn (unk1, unk2) @ 0x5078;
        fn (unk) @ 0x5079;
        fn (unk) @ 0x507A;
        fn (unk1, unk2, unk3) @ 0x507B;
        fn () @ 0x507C;
        fn (unk1, unk2) @ 0x507D;
        fn (unk1, unk2) @ 0x507E;
        fn (unk) @ 0x5080;
        fn (unk) @ 0x5081;
        fn (unk) @ 0x5082;
        fn (unk) @ 0x5083;
        fn (unk) @ 0x5084;
        fn (unk) @ 0x5085;
        fn () @ 0x5086;
        fn () @ 0x5087;
        fn (unk1, unk2) @ 0x5088;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x5089;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x508B;
        fn (unk) @ 0x508D;
        fn (unk) @ 0x508E;
        fn (unk) @ 0x508F;
        fn (unk) @ 0x5090;
    };

    pub const cam: [Func; 137] = funcs! {
        fn sleep(unk) @ 0x6000;
        fn wakeUp(unk) @ 0x6001;
        fn setPos(unk1, unk2, unk3) @ 0x6002;
        fn getPos(unk1, unk2, unk3) @ 0x6003;
        fn setPolar(unk1, unk2, unk3) @ 0x6004;
        fn setPolarOffset(unk1, unk2, unk3) @ 0x6005;
        fn setHypot(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6006;
        fn setHypot2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6007;
        fn setHypot3(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6008;
        fn setAct(unk1, unk2, unk3) @ 0x6009;
        fn setFilter(unk1, unk2, unk3, unk4, unk5) @ 0x600A;
        fn setFilter2(unk1, unk2, unk3, unk4, unk5) @ 0x600B;
        fn setFilterY(unk1, unk2, unk3) @ 0x600C;
        fn setFilterY2(unk1, unk2, unk3) @ 0x600D;
        fn sleepFilter(unk) @ 0x600E;
        fn resetFilter() @ 0x600F;
        fn move(unk) @ 0x6010;
        fn movePolar(unk) @ 0x6011;
        fn moveCos(unk) @ 0x6012;
        fn movePolarCos(unk) @ 0x6013;
        fn moveAcc(unk1, unk2, unk3, unk4) @ 0x6014;
        fn movePolarAcc(unk1, unk2, unk3, unk4) @ 0x6015;
        fn resetMove() @ 0x6016;
        fn setInertia(unk1, unk2, unk3, unk4) @ 0x6017;
        fn setDirVector(unk1, unk2, unk3) @ 0x6018;
        fn resetDirVector() @ 0x6019;
        fn wait() @ 0x601A;
        fn check() @ 0x601B;
        fn setDataPoint(unk1, unk2) @ 0x601C;
        fn setDataPointHypot(unk1, unk2, unk3, unk4) @ 0x601D;
        fn setDataPoint2(unk1, unk2) @ 0x601E;
        fn setDataPointHypot2(unk1, unk2, unk3, unk4) @ 0x601F;
        fn refSetPos(unk1, unk2, unk3) @ 0x6020;
        fn refGetPos(unk1, unk2, unk3) @ 0x6021;
        fn refSetPolar(unk1, unk2, unk3) @ 0x6022;
        fn refSetPolarOffset(unk1, unk2, unk3) @ 0x6023;
        fn refSetHypot(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6024;
        fn refSetHypot2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6025;
        fn refSetHypot3(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6026;
        fn refSetAct(unk1, unk2, unk3) @ 0x6027;
        fn refSetFilter(unk1, unk2, unk3, unk4, unk5) @ 0x6028;
        fn refSetFilter2(unk1, unk2, unk3, unk4, unk5) @ 0x6029;
        fn refSetFilterY(unk1, unk2, unk3) @ 0x602A;
        fn refSetFilterY2(unk1, unk2, unk3) @ 0x602B;
        fn refSleepFilter(unk) @ 0x602C;
        fn refResetFilter() @ 0x602D;
        fn refMove(unk) @ 0x602E;
        fn refMovePolar(unk) @ 0x602F;
        fn refMoveCos(unk) @ 0x6030;
        fn refMovePolarCos(unk) @ 0x6031;
        fn refMoveAcc(unk1, unk2, unk3, unk4) @ 0x6032;
        fn refMovePolarAcc(unk1, unk2, unk3, unk4) @ 0x6033;
        fn refResetMove() @ 0x6034;
        fn refSetInertia(unk1, unk2, unk3, unk4) @ 0x6035;
        fn refSetDirVector(unk1, unk2, unk3, unk4) @ 0x6036;
        fn refResetDirVector() @ 0x6037;
        fn refWait() @ 0x6038;
        fn refCheck() @ 0x6039;
        fn setRoll(unk) @ 0x603A;
        fn setScrDpt(unk) @ 0x603B;
        fn setAct2(unk1, unk2, unk3, unk4) @ 0x603C;
        fn refSetAct2(unk1, unk2, unk3, unk4) @ 0x603D;
        fn setBtl(unk1, unk2, unk3) @ 0x603E;
        fn refSetBtl(unk1, unk2, unk3) @ 0x603F;
        fn setBtlPolar(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6040;
        fn refSetBtlPolar(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6041;
        fn refMoveStat(unk) @ 0x6042;
        fn moveStat(unk) @ 0x6043;
        fn setBtlPolar2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6044;
        fn refSetBtlPolar2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6045;
        fn setSpline(unk1, unk2, unk3, unk4) @ 0x6046;
        fn refSetSpline(unk1, unk2, unk3, unk4) @ 0x6047;
        fn startSpline() @ 0x6048;
        fn regSpline() @ 0x6049;
        fn refStartSpline() @ 0x604A;
        fn refRegStartSpline() @ 0x604B;
        fn setChrPolar(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x604C;
        fn setChrPolar2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x604D;
        fn scrSet(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x604E;
        fn scrOff(unk) @ 0x604F;
        fn drawSet(unk1, unk2, unk3, unk4, unk5) @ 0x6050;
        fn drawLink(unk1, unk2) @ 0x6051;
        fn scrLink(unk1, unk2) @ 0x6052;
        fn scrMove(unk1, unk2) @ 0x6053;
        fn scrMoveCos(unk1, unk2) @ 0x6054;
        fn scrMoveAcc(unk1, unk2, unk3, unk4) @ 0x6055;
        fn drawMove(unk1, unk2) @ 0x6056;
        fn drawMoveCos(unk1, unk2) @ 0x6057;
        fn drawMoveAcc(unk1, unk2, unk3, unk4) @ 0x6058;
        fn refSetSplineFilter(unk1, unk2) @ 0x6059;
        fn refSetSplineFilter2(unk1, unk2, unk3) @ 0x605A;
        fn setSpline2(unk1, unk2) @ 0x605B;
        fn refSetShake(unk1, unk2, unk3, unk4, unk5) @ 0x605C;
        fn setShake(unk1, unk2, unk3, unk4, unk5) @ 0x605D;
        fn setScreenShake(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x605E;
        fn refResetShake() @ 0x605F;
        fn resetShake() @ 0x6060;
        fn resetScreenShake(unk) @ 0x6061;
        fn refWaitShake() @ 0x6062;
        fn waitShake() @ 0x6063;
        fn waitScreenShake(unk) @ 0x6064;
        fn priority(unk) @ 0x6065;
        fn refSetShakeB(unk1, unk2, unk3, unk4, unk5) @ 0x6066;
        fn setShakeB(unk1, unk2, unk3, unk4, unk5) @ 0x6067;
        fn setScreenShakeB(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6068;
        fn refSetShake2(unk1, unk2, unk3, unk4, unk5) @ 0x6069;
        fn refSetShake2(unk1, unk2, unk3, unk4, unk5) @ 0x606A;
        fn setScreenShake2(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x606B;
        fn refSetShake2B(unk1, unk2, unk3, unk4, unk5) @ 0x606C;
        fn refSetShake2B(unk1, unk2, unk3, unk4, unk5) @ 0x606D;
        fn setScreenShake2B(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x606E;
        fn refSetShake3(unk1, unk2, unk3, unk4, unk5) @ 0x606F;
        fn refSetShake3(unk1, unk2, unk3, unk4, unk5) @ 0x6070;
        fn setScreenShake3(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6071;
        fn refSetShake3B(unk1, unk2, unk3, unk4, unk5) @ 0x6072;
        fn refSetShake3B(unk1, unk2, unk3, unk4, unk5) @ 0x6073;
        fn setScreenShake3B(unk1, unk2, unk3, unk4, unk5, unk6) @ 0x6074;
        fn scrSetCam(unk1, unk2) @ 0x6075;
        fn freeBattle() @ 0x6076;
        fn getRoll() -> Float @ 0x6077;
        fn getScrDpt() -> Float @ 0x6078;
        fn scrResetMove(unk) @ 0x6079;
        fn drawResetMove(unk) @ 0x607A;
        fn scrWait(unk) @ 0x607B;
        fn drawWait(unk) @ 0x607C;
        fn blur(unk) @ 0x607D;
        fn focus(unk) @ 0x607E;
        fn setFocus(unk1, unk2) @ 0x607F;
        fn rand(unk) @ 0x6080;
        fn refSetShake4(unk1, unk2, unk3, unk4, unk5) @ 0x6081;
        fn setShake4(unk1, unk2, unk3, unk4, unk5) @ 0x6082;
        fn refSetShake5(unk1, unk2, unk3, unk4, unk5) @ 0x6083;
        fn setShake5(unk1, unk2, unk3, unk4, unk5) @ 0x6084;
        fn getRealPos(unk1, unk2, unk3) @ 0x6085;
        fn refGetRealPos(unk1, unk2, unk3) @ 0x6086;
        fn refReset() @ 0x6087;
        fn reset() @ 0x6088;
    };

    pub const battle: [Func; 311] = funcs! {
        fn stopAction() @ 0x7000;
        fn setRandPos(value: Bool) @ 0x7001;
        fn launchBattle(encounter, should_transition: Bool) @ 0x7002;
        fn dirTarget(unk1, unk2) @ 0x7003;
        fn setDirRate(unk: Float) @ 0x7004;
        fn isWater() -> Bool @ 0x7005;
        fn dirBasic(unk1, unk2) @ 0x7006;
        fn startMotion(unk: Int) @ 0x7007;
        fn awaitMotion() @ 0x7008;
        fn setHasGravity(unk: Bool) @ 0x7009;
        fn setHeight(unk1: Int, unk2: Float) @ 0x700A;
        fn performMove(target: Enum(Target), move_id) -> Bool @ 0x700B;
        fn performMove(target: Enum(Character), move_id) -> Bool @ 0x700B;
        fn move(unk1, unk2, unk3, unk4, unk5, unk6, unk7, unk8) @ 0x700C;
        fn dirPos(unk1, unk2) @ 0x700D;
        fn setDamage(unk) @ 0x700E;
        fn getCharProp(actor: Enum(Target), prop: Enum(ActorProperty)) @ 0x700F;
        fn getCharProp(actor: Enum(Character), prop: Enum(ActorProperty)) @ 0x700F;
        fn defineActorSubset(group: Enum(Target), prop: Enum(ActorProperty), always_zero, selector: Enum(Selector)) @ 0x7010;
        fn defineActorSubset(group: Enum(Character), prop: Enum(ActorProperty), always_zero, selector: Enum(Selector)) @ 0x7010;
        fn camMode(unk) @ 0x7011;
        fn stopEffect() @ 0x7012;
        fn setChrSpeed(x: Float) @ 0x7013;
        fn getLastCommand() -> Int @ 0x7014;
        fn printInt(n: Int) @ 0x7015;
        fn stopMotion() @ 0x7016;
        fn setNormalEffect(unk1, unk2) @ 0x7017;
        fn setCharProp(prop: Enum(ActorProperty), value) @ 0x7018;
        fn getLastMove() -> Int @ 0x7019;
        fn getMoveProp(move_id, prop: Enum(MoveProperty)) @ 0x701A;
        fn overrideAttemptedAction(target: Enum(Target), move_id) @ 0x701B;
        fn overrideAttemptedAction(target: Enum(Character), move_id) @ 0x701B;
        fn setMotionLevel(value) @ 0x701C;
        fn getMotionLevel() @ 0x701D;
        fn countActorsIn(within: Enum(Target), actor: Enum(Target)) -> Int @ 0x701E;
        fn countActorsIn(within: Enum(Target), actor: Enum(Character)) -> Int @ 0x701E;
        fn chgWaitMotion(unk) @ 0x701F;
        fn checkStartEffect() @ 0x7020;
        fn derefChar(actor: Enum(Character)) @ 0x7021;
        fn setAmbushState(state: Enum(AmbushState)) @ 0x7022;
        fn distTarget(unk) @ 0x7023;
        fn getCurrentEncounterIdx() -> Int @ 0x7024;
        fn defineActorSubsetFree(group: Enum(Target), prop: Enum(ActorProperty), always_zero, selector: Enum(Selector)) @ 0x7025;
        fn defineActorSubsetFree(group: Enum(Character), prop: Enum(ActorProperty), always_zero, selector: Enum(Selector)) @ 0x7025;
        fn setWeak(value) @ 0x7026;
        fn getWeak() @ 0x7027;
        fn setScale(x: Float, y: Float, z: Float) @ 0x7028;
        fn setFloating(floating: Bool) @ 0x7029;
        fn getBtlPos() @ 0x702A;
        fn checkMotion() @ 0x702B;
        fn setHoming(unk1, unk2, unk3, unk4, unk5, unk6, unk7, unk8, unk9) @ 0x702C;
        fn resetMove() @ 0x702D;
        fn moveTargetDist(unk) -> Float @ 0x702E;
        fn out(unk) @ 0x702F;
        fn getMoveFlag() @ 0x7030;
        fn startMotion() @ 0x7031;
        fn setActorFacingAngle(angle: Float) @ 0x7032;
        fn setEnMapID(unk) @ 0x7033;
        fn endBattle(end_type: Enum(BattleEndType)) @ 0x7034;
        fn getBattleEndType() -> Int @ 0x7035;
        fn setTrans(unk1, unk2, unk3) @ 0x7036;
        fn addMove(actor: Enum(Target), move_id) @ 0x7037;
        fn addMove(actor: Enum(Character), move_id) @ 0x7037;
        fn removeMove(actor: Enum(Target), move_id) @ 0x7038;
        fn removeMove(actor: Enum(Character), move_id) @ 0x7038;
        fn stopDeath() @ 0x7039;
        fn setSpeed(unk) @ 0x703A;
        fn setMoveUsability(actor: Enum(Target), move_id, disabled: Bool) @ 0x703B;
        fn setMoveUsability(actor: Enum(Character), move_id, disabled: Bool) @ 0x703B;
        fn runBattleScriptA(index: Int) @ 0x703C;
        fn on() @ 0x703D;
        fn wait() @ 0x703E;
        fn requestCam(unk1, unk2) @ 0x703F;
        fn startMagic(unk) @ 0x7040;
        fn endMagic() @ 0x7041;
        fn displayMessage(box_idx: Int, string, unk1, unk2, align: Enum(TextAlignment)) @ 0x7042;
        fn awaitMessage(box_idx: Int) @ 0x7043;
        fn closeMessage(box_idx: Int) @ 0x7044;
        fn distTargetFrame(unk) @ 0x7045;
        fn splineStart(unk) @ 0x7046;
        fn splineRegist(unk1, unk2) @ 0x7047;
        fn splineRegistPos(unk1, unk2, unk3, unk4) @ 0x7048;
        fn splineMove(unk1, unk2, unk3, unk4) @ 0x7049;
        fn checkMove(unk) -> Int @ 0x704A;
        fn requestMotion(unk1, unk2, unk3) @ 0x704B;
        fn awaitRequestMotion(unk) @ 0x704C;
        fn setDeathLevel(value) @ 0x704D;
        fn setDeathAnim(anim: Enum(DeathAnim)) @ 0x704E;
        fn setEventCharFlag(unk1, unk2) @ 0x704F;
        fn reinit(actor: Enum(Target)) @ 0x7050;
        fn reinit(actor: Enum(Character)) @ 0x7050;
        fn awaitNormalEffect() @ 0x7051;
        fn attachActor(child: Enum(Character), parent: Enum(Character), attachment_point: Int) @ 0x7052;
        fn moveJump(unk1, unk2, unk3, unk4, unk5, unk6, unk7, unk8, unk9) @ 0x7053;
        fn setCharPosElem(unk1, unk2, unk3) @ 0x7054;
        fn setBodyHit(unk) @ 0x7055;
        fn setSpecialBattle(unk) @ 0x7056;
        fn dirMove(unk1, unk2, unk3, unk4) @ 0x7057;
        fn checkMotionNum(unk1, unk2) @ 0x7058;
        fn moveTargetDist2D(unk) @ 0x7059;
        fn forcePerformMove(target: Enum(Character), move_id) -> Bool @ 0x705A;
        fn getCamWidth(unk) -> Float @ 0x705B;
        fn getCamHeight(unk) -> Float @ 0x705C;
        fn setBindEffect(unk1, unk2) @ 0x705D;
        fn resetBindEffect() @ 0x705E;
        fn printFloat(x: Float) @ 0x705F;
        fn setStatEffect() @ 0x7060;
        fn clearStatEffect() @ 0x7061;
        fn setHitEffect(unk1, unk2) @ 0x7062;
        fn awaitHitEffect() @ 0x7063;
        fn loadVoice(voice_idx: Int) @ 0x7064;
        fn startVoice() @ 0x7065;
        fn stopVoice() @ 0x7066;
        fn getVoiceStatus() @ 0x7067;
        fn syncVoice() @ 0x7068;
        fn searchCharCam(unk1, unk2, unk3, unk4) @ 0x7069;
        fn checkOwnTarget(unk) @ 0x706A;
        fn setModelHide(unk1, unk2, unk3) @ 0x706B;
        fn sfxNormal(unk1, unk2) @ 0x706C;
        fn soundStreamNormal(unk1, unk2) @ 0x706D;
        fn requestVoice(unk1, unk2) @ 0x706E;
        fn setMotion2(unk) @ 0x706F;
        fn statusOn() @ 0x7070;
        fn statusOff() @ 0x7071;
        fn displayDialog(box_idx: Int, string) @ 0x7072;
        fn attachWeapon(unk) @ 0x7073;
        fn detachWeapon(unk) @ 0x7074;
        fn requestWeaponMotion(unk1, unk2, unk3) @ 0x7075;
        fn ballSplineMove(unk1, unk2, unk3) @ 0x7076;
        fn distTargetFrameBall(unk1, unk2) @ 0x7077;
        fn getMovePropForActor(prop: Enum(MoveProperty), actor: Enum(Character)) @ 0x7078;
        fn resetWeapon() @ 0x7079;
        fn getCalcResult(unk) @ 0x707A;
        fn sfx(unk1, unk2) @ 0x707B;
        fn awaitSound() @ 0x707C;
        fn setDebug(debug_flag, active: Bool) @ 0x707D;
        fn getDebug(unk) @ 0x707E;
        fn setBtlPos(unk) @ 0x707F;
        fn changeAuron(unk) @ 0x7080;
        fn awaitExe() @ 0x7081;
        fn setFreeEffect(unk1, unk2) @ 0x7082;
        fn setAfterImage(unk1, unk2) @ 0x7083;
        fn resetAfterImage() @ 0x7084;
        fn moveAttack(unk1, unk2, unk3, unk4, unk5, unk6, unk7, unk8) @ 0x7085;
        fn useCharMpLimit() @ 0x7086;
        fn fadeSfx(unk1, unk2, unk3) @ 0x7087;
        fn regSfx(unk1, unk2) @ 0x7088;
        fn regSfxFade(unk1, unk2, unk3) @ 0x7089;
        fn initEncounter(unk) @ 0x708A;
        fn getEncounter(unk) @ 0x708B;
        fn setEncounterEnabled(encounter, active: Bool) @ 0x708C;
        fn getLastActionChar() @ 0x708D;
        fn checkBtlPos2() @ 0x708E;
        fn dirPosBasic(unk) @ 0x708F;
        fn setCritEffect(unk) @ 0x7090;
        fn changeCharName(unk1, unk2) @ 0x7091;
        fn getGroundDist(unk) @ 0x7092;
        fn checkDirFlag() @ 0x7093;
        fn setTransVisible(unk1, unk2, unk3) @ 0x7094;
        fn getMoveFrameRest() @ 0x7095;
        fn getReflect() @ 0x7096;
        fn runBattleScriptB(index: Int) @ 0x7097;
        fn checkDefenseMotion() @ 0x7098;
        fn setCursorType(unk) @ 0x7099;
        fn checkPoison() @ 0x709A;
        fn getCharPosY(unk) -> Float @ 0x709B;
        fn getTargetDir(unk1, unk2) -> Float @ 0x709C;
        fn _awaitMotion() @ 0x709D;
        fn setMotionSignal(unk1, unk2, unk3) @ 0x709E;
        fn getCharTargetDir(unk) -> Float @ 0x709F;
        fn setUpVectorFlag(unk) @ 0x70A0;
        fn derefEnemy(actor: Enum(Character)) @ 0x70A1;
        fn readMotion(unk) @ 0x70A2;
        fn overrideMotion(unk) @ 0x70A3;
        fn disposeMotion() @ 0x70A4;
        fn setMapCenter(unk1, unk2, unk3) @ 0x70A5;
        fn setEscape(unk) @ 0x70A6;
        fn getMotionData(unk1, unk2) -> Float @ 0x70A7;
        fn setMotionData(unk1, unk2, unk3) @ 0x70A8;
        fn awaitVoice(unk) @ 0x70A9;
        fn getOwnProp(prop: Enum(ActorProperty)) @ 0x70AA;
        fn setOwnProp(prop: Enum(ActorProperty)) @ 0x70AB;
        fn getMotionData2(unk) -> Float @ 0x70AC;
        fn checkWakkaWeapon() @ 0x70AD;
        fn getLastDeathChar() @ 0x70AE;
        fn getVoiceFlag() @ 0x70AF;
        fn distTargetFrame2(unk) @ 0x70B0;
        fn printSp(unk) @ 0x70B1;
        fn setMotionData2(unk1, unk2) @ 0x70B2;
        fn setVoice(unk) @ 0x70B3;
        fn fadeOutWeapon() @ 0x70B4;
        fn resetMotionSpeed() @ 0x70B5;
        fn distTargetFrameSpeed(unk) @ 0x70B6;
        fn mesa(unk1, unk2) @ 0x70B7;
        fn setSkipMode(unk) @ 0x70B8;
        fn getCamWidth2(unk) -> Float @ 0x70B9;
        fn getCamHeight2(unk) -> Float @ 0x70BA;
        fn moveLeave(unk) @ 0x70BB;
        fn awaitNomEff(unk) @ 0x70BC;
        fn awaitHitEff(unk) @ 0x70BD;
        fn getCharDir(unk) -> Float @ 0x70BE;
        fn setBindScale(unk) @ 0x70BF;
        fn getHeight(unk) -> Float @ 0x70C0;
        fn distTarget2(unk1, unk2) @ 0x70C1;
        fn getTargetDirH(unk1, unk2) -> Float @ 0x70C2;
        fn getCharTargetDir2(unk) -> Float @ 0x70C3;
        fn equipWakkaWeapon(unk) @ 0x70C4;
        fn checkRetBtlPos() @ 0x70C5;
        fn getCamBuffer(unk) -> Float @ 0x70C6;
        fn getCamBufferFloat(unk) -> Float @ 0x70C7;
        fn sfx2(unk1, unk2) @ 0x70C8;
        fn sfx3(unk1, unk2) @ 0x70C9;
        fn regSfx2(unk1, unk2) @ 0x70CA;
        fn regSfx3(unk1, unk2) @ 0x70CB;
        fn initPredefinedGroup(actor: Enum(Target)) @ 0x70CC;
        fn initPredefinedGroup(actor: Enum(Character)) @ 0x70CC;
        fn addToPredefinedGroup(actor: Enum(Target)) @ 0x70CD;
        fn addToPredefinedGroup(actor: Enum(Character)) @ 0x70CD;
        fn removeFromPredefinedGroup(actor: Enum(Target)) @ 0x70CE;
        fn removeFromPredefinedGroup(actor: Enum(Character)) @ 0x70CE;
        fn getReverb() @ 0x70CF;
        fn setCameraSelectMode(unk) @ 0x70D0;
        fn getNomEff(unk) @ 0x70D1;
        fn getHitEff(unk) @ 0x70D2;
        fn setNomEff(unk1, unk2, unk3) @ 0x70D3;
        fn setHitEff(unk1, unk2, unk3) @ 0x70D4;
        fn setSummoner(actor: Enum(Character)) @ 0x70D5;
        fn getGuessedDmg(user_id, target_id, command_id) -> Int @ 0x70D6;
        fn setDmgMotion(unk1, unk2) @ 0x70D7;
        fn setAnimaChainOff(unk) @ 0x70D8;
        fn exeAnimaChainOff() @ 0x70D9;
        fn getAmbushState() -> Int @ 0x70DA;
        fn getAnimaChainOff() @ 0x70DB;
        fn changeActorName(actor: Enum(Character), string) @ 0x70DC;
        fn setDebugCount(unk) @ 0x70DD;
        fn areSubsEnabled() -> Bool @ 0x70DE;
        fn checkBtlScene(unk) @ 0x70DF;
        fn isNotCounter() -> Bool @ 0x70E0;
        fn getNormalAttack() @ 0x70E1;
        fn setTexAnime(unk1, unk2) @ 0x70E2;
        fn getEffectMemory(unk) @ 0x70E3;
        fn getCalcResultLimit(unk1, unk2) @ 0x70E4;
        fn setNomEffReg(unk1, unk2, unk3) @ 0x70E5;
        fn setHitEffReg(unk1, unk2, unk3) @ 0x70E6;
        fn setRandomTarget(unk) @ 0x70E7;
        fn getTotalGil() -> Int @ 0x70E8;
        fn getYojimboHireType() -> Int @ 0x70E9;
        fn setYojimboHireType(type: Int) @ 0x70EA;
        fn getYojimboRng() @ 0x70EB;
        fn getItemCount(item_id: Int) @ 0x70EC;
        fn giveItem(item_id: Int, quantity: Int) @ 0x70ED;
        fn getRandomYojimboMove(motivation: Int, unk) -> Int @ 0x70EE;
        fn setEffSignal(unk1, unk2) @ 0x70EF;
        fn getCamCount() -> Float @ 0x70F0;
        fn commandClear() @ 0x70F1;
        fn commandSet(move_id: Int) @ 0x70F2;
        fn getRandomMagusMove(unk, chance: Int) -> Bool @ 0x70F3;
        fn getCommandTarget(unk1, unk2) @ 0x70F4;
        fn checkUseCommand(unk1, unk2) @ 0x70F5;
        fn initCommandBuffer() @ 0x70F6;
        fn setCommandBuffer(unk) @ 0x70F7;
        fn getCommandBuffer() @ 0x70F8;
        fn searchChr3(unk1, unk2, unk3, unk4, unk5) @ 0x70F9;
        fn setRandomMagusMove(unk) @ 0x70FA;
        fn getCommandTargetSearch(unk1, unk2, unk3, unk4, unk5) @ 0x70FB;
        fn getRandomMagusMove() @ 0x70FC;
        fn setUpLimit(unk1, unk2) @ 0x70FD;
        fn setUpLimi2(unk1, unk2, unk3, unk4) @ 0x70FE;
        fn setDeltaTarget() @ 0x70FF;
        fn checkRequestMotion(unk) @ 0x7100;
        fn getFullCommand() @ 0x7101;
        fn makeActorFaceActor(actor: Enum(Target), target: Enum(Target)) @ 0x7102;
        fn makeActorFaceActor(actor: Enum(Target), target: Enum(Character)) @ 0x7102;
        fn makeActorFaceActor(actor: Enum(Character), target: Enum(Target)) @ 0x7102;
        fn makeActorFaceActor(actor: Enum(Character), target: Enum(Character)) @ 0x7102;
        fn makeActorFaceCoords(actor: Enum(Target), x: Float, y: Float, z: Float) @ 0x7103;
        fn makeActorFaceCoords(actor: Enum(Character), x: Float, y: Float, z: Float) @ 0x7103;
        fn changeMoveAnim(move_id: Int, anim1: Int, anim2: Int) @ 0x7104;
        fn awaitPetrification() @ 0x7105;
        fn doesActorKnowMove(actor: Enum(Target), move_id: Int) -> Bool @ 0x7106;
        fn doesActorKnowMove(actor: Enum(Character), move_id: Int) -> Bool @ 0x7106;
        fn dirPosBasic2(unk) @ 0x7107;
        fn dirBasic2(unk1, unk2) @ 0x7108;
        fn setAppear(unk1, unk2, unk3) @ 0x7109;
        fn setSummonTiming() @ 0x710A;
        fn awaitSummontiming() @ 0x710B;
        fn stopPetrification() @ 0x710C;
        fn defensePosOff() @ 0x710D;
        fn getWakkaLimitSkill() @ 0x710E;
        fn getWakkaLimitNum() @ 0x710F;
        fn mouseOn(unk) @ 0x7110;
        fn mouseOff(unk) @ 0x7111;
        fn dirMove2(unk1, unk2, unk3, unk4) @ 0x7112;
        fn monsterFarm() @ 0x7113;
        fn sphereMonitor() @ 0x7114;
        fn dirResetLeave() @ 0x7115;
        fn setSummonDefenseEffect() @ 0x7116;
        fn overrideDeathAnimWithMove(target: Enum(Target), move_id: Int) @ 0x7117;
        fn overrideDeathAnimWithMove(actor: Enum(Character), move_id: Int) @ 0x7117;
        fn setSummonGameOver(unk) @ 0x7118;
        fn setCounterFlag(counter: Bool) @ 0x7119;
        fn setWind(unk1, unk2, unk3, unk4) @ 0x711A;
        fn setCameraStandard() @ 0x711B;
        fn setGameOverEffNum(effect_num: Int) @ 0x711C;
        fn setShadowHeight(height: Float) @ 0x711D;
        fn displayMessageSimple(box_idx: Int, string) @ 0x7120;
        fn (unk) @ 0x7123;
        fn (unk) @ 0x7124;
        fn () @ 0x7125;
        fn (unk) @ 0x7126;
        fn (unk1, unk2) @ 0x7127;
    };

    pub const map: [Func; 63] = funcs! {
        fn setLevelLayerVisibility(layer_idx: Int, visible: Bool) @ 0x8000;
        fn setSkyboxVisibility(visible: Bool) @ 0x8001;
        fn setGfxActive(gfx_idx: Int, active: Bool) @ 0x8002;
        fn startGfxTimer(gfx_idx: Int) @ 0x8003;
        fn awaitGfxStopped(gfx_idx: Int) @ 0x8004;
        fn awaitGfxEnded(gfx_idx: Int, await_children: Bool) @ 0x8005;
        fn (unk) @ 0x8006;
        fn setAllGfxActive() @ 0x8007;
        fn stopAllGfx() @ 0x8008;
        fn bindGfxToTarget(gfx_idx: Int, target: Int) @ 0x8009;
        fn bindGfxToPos(gfx_idx: Int, x: Float, y: Float, z: Float) @ 0x800A;
        fn unbindGfx(gfx_idx: Int) @ 0x800B;
        fn setGfxGlobalEnabled(enabled: Bool) @ 0x800C;
        fn setGfxVisibility(gfx_idx: Int, visible: Bool) @ 0x800D;
        fn (unk) @ 0x800F;
        fn (unk) @ 0x8010;
        fn (unk1, unk2, unk3) @ 0x8011;
        fn (unk1, unk2) @ 0x8014;
        fn (unk1, unk2) @ 0x801D;
        fn (unk) @ 0x801E;
        fn (unk) @ 0x801F;
        fn (unk) @ 0x8020;
        fn (unk) @ 0x8021;
        fn (unk1, unk2) @ 0x8022;
        fn (unk1, unk2, unk3, unk4, unk5) @ 0x8026;
        fn () @ 0x802C;
        fn () @ 0x802D;
        fn (unk1, unk2) @ 0x802E;
        fn (unk) @ 0x802F;
        fn (unk1, un2k) @ 0x8030;
        fn (unk) @ 0x8032;
        fn setGfxGroupActive(group_id: Int, active: Bool) @ 0x8035;
        fn setGfxGroupVisibility(group_id: Int, visible: Bool) @ 0x8036;
        fn (unk1, unk2, unk3) @ 0x8037;
        fn (unk) @ 0x8038;
        fn () @ 0x8039;
        fn (unk) @ 0x803A;
        fn () @ 0x803B;
        fn (unk) @ 0x803C;
        fn () @ 0x803D;
        fn setFogColor(r: Int, g: Int, b: Int) @ 0x803E;
        fn setFogRed(r: Int) @ 0x803F;
        fn setFogGreen(g: Int) @ 0x8040;
        fn setFogBlue(b: Int) @ 0x8041;
        fn getFogRed() -> Int @ 0x8042;
        fn getFogGreen() -> Int @ 0x8043;
        fn getFogBlue() -> Int @ 0x8044;
        fn setClearColor(r: Int, g: Int, b: Int) @ 0x8045;
        fn () @ 0x8049;
        fn () @ 0x804A;
        fn () @ 0x804B;
        fn (unk1, unk2, unk3) @ 0x804F;
        fn bindGfxToMapGroup(gfx_idx: Int, group_idx: Int) @ 0x8059;
        fn setGfxPosition(gfx_idx: Int, x: Float, y: Float, z: Float) @ 0x805B;
        fn (unk) @ 0x805C;
        fn () @ 0x805D;
        fn (unk) @ 0x805E;
        fn stopGfx(gfx_idx: Int) @ 0x805F;
        fn stopGfxGroup(group_idx: Int) @ 0x8060;
        fn setGfxGlobalPaused(paused: Bool) @ 0x8066;
        fn (unk1, unk2, unk3, unk4) @ 0x8067;
        fn (unk) @ 0x806A;
        fn (unk) @ 0x806B;
    };

    pub const mount: [Func; 0] = [];
    pub const unkA: [Func; 0] = [];

    pub const movie: [Func; 16] = funcs! {
        fn startMovie(movie_id: Int, unk2: Int) @ 0xB000;
        fn stopMovieMaybe() @ 0xB001;
        fn getCurrentFrame() -> Int @ 0xB002;
        fn () @ 0xB003;
        fn stopMovieMaybeTwo() @ 0xB004;
        fn () @ 0xB005;
        fn () @ 0xB006;
        fn () @ 0xB007;
        fn () @ 0xB008;
        fn unpauseMovieMaybe() @ 0xB009;
        fn playMovie(movie_id: Int, unk2: Int) @ 0xB00A;
        fn () @ 0xB00B;
        fn getLastFrame() -> Int @ 0xB00C;
        fn disableDrawingChr() @ 0xB00D;
        fn () @ 0xB00E;
        fn () @ 0xB00F;
    };

    pub const debug: [Func; 31] = funcs! {
        fn (unk) @ 0xC002;
        fn (unk) @ 0xC003;
        fn (unk1, unk2) @ 0xC007;
        fn (unk) @ 0xC009;
        fn (unk) @ 0xC00B;
        fn (unk) @ 0xC00C;
        fn (unk) @ 0xC00D;
        fn () @ 0xC013;
        fn () @ 0xC014;
        fn (unk) @ 0xC018;
        fn (unk) @ 0xC022;
        fn launchBattleAlwaysWin(encounter: Int, transition: Enum(BattleTrans)) @ 0xC024;
        fn (unk) @ 0xC025;
        fn () @ 0xC028;
        fn (unk) @ 0xC02A;
        fn (unk) @ 0xC02C;
        fn (unk1, unk2) @ 0xC02F;
        fn () @ 0xC030;
        fn (unk) @ 0xC031;
        fn (unk) @ 0xC036;
        fn () @ 0xC03B;
        fn () @ 0xC03C;
        fn (unk) @ 0xC051;
        fn () @ 0xC052;
        fn (unk1, unk2, unk3, unk4) @ 0xC053;
        fn (unk) @ 0xC054;
        fn (unk) @ 0xC055;
        fn (unk1, unk2, unk3) @ 0xC056;
        fn (unk) @ 0xC057;
        fn (unk) @ 0xC058;
        fn (unk) @ 0xC05B;
    };

    pub const abiMap: [Func; 0] = [];
    pub const unkE: [Func; 0] = [];
    pub const unkF: [Func; 0] = [];
}
