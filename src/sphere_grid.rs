use bitflags::bitflags;

bitflags! {
    /// Bitfield of node types which spheres can activate
    pub struct NodeTypes: u16 {
        const strength = 0x0001;
        const defense = 0x0002;
        const magic = 0x0004;
        const magic_defense = 0x0008;
        const agility = 0x0010;
        const luck = 0x0020;
        const evasion = 0x0040;
        const accuracy = 0x0080;
        const hp = 0x0100;
        const mp = 0x0200;
        const ability = 0x0400;
    }
}

pub enum GridType {
    Original,
    Standard,
    Expert,
}

#[repr(u8)]
/// What kind of sphere is this?
pub enum SphereRole {
    Power = 0x00,
    Mana = 0x01,
    Speed = 0x02,
    Ability = 0x03,
    Fortune = 0x04,
    Special = 0x05,
    Skill = 0x06,
    WhiteMagic = 0x07,
    BlackMagic = 0x08,
    Master = 0x09,
    Lv1Key = 0x0A,
    Lv2Key = 0x0B,
    Lv3Key = 0x0C,
    Lv4Key = 0x0D,
    Luck = 0x0E,
    Hp = 0x0F,
    Mp = 0x10,
    Return = 0x11,
    Friend = 0x12,
    Teleport = 0x13,
    Warp = 0x14,
    Strength = 0x15,
    Defense = 0x16,
    Magic = 0x17,
    MagicDefense = 0x18,
    Agility = 0x19,
    Evasion = 0x1A,
    Accuracy = 0x1B,
    Clear = 0x1C,
    Attribute = 0x1D,
}

#[repr(u8)]
/// What does this sphere do?
pub enum SphereType {
    None = 0x0,
    /// Sphere activates nodes on the sphere grid
    Activation = 0x1,
    /// Sphere adds or removes nodes on the sphere grid
    Modification = 0x2,
}
