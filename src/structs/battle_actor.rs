use bitflags::bitflags;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

bitflags!(
    #[derive(Debug)]
    #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
    /// Bitfield of auto-statuses a battle actor can have
    pub struct AutoStatus: u32 {
        const death =           0x00000001;
        const zombie =          0x00000002;
        const petrify =         0x00000004;
        const poison =          0x00000008;
        const power_break =     0x00000010;
        const magic_break =     0x00000020;
        const armor_break =     0x00000040;
        const mental_break =    0x00000080;
        const confuse =         0x00000100;
        const berserk =         0x00000200;
        const provoke =         0x00000400;
        const threaten =        0x00000800;
        const sleep =           0x00001000;
        const silence =         0x00002000;
        const darkness =        0x00004000;
        const unk1 =            0x00008000;
        const unk2 =            0x00010000;
        const unk3 =            0x00020000;
        const unk4 =            0x00040000;
        const shell =           0x00080000;
        const protect =         0x00100000;
        const reflect =         0x00200000;
        const nul_water =       0x00400000;
        const nul_fire =        0x00800000;
        const nul_thunder =     0x01000000;
        const nul_ice =         0x02000000;
        const regen =           0x04000000;
        const haste =           0x08000000;
        const slow =            0x10000000;
    }

    #[derive(Debug)]
    #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
    /// Bitfield of additional immunities a battle actor can have
    pub struct ExtraImmunities: u16 {
        const scan =            0x0001;
        const distill_power =   0x0002;
        const distill_mana =    0x0004;
        const distill_speed =   0x0008;
        const distill_unused =  0x0010;
        const distill_ability = 0x0020;
        const shield =          0x0040;
        const boost =           0x0080;
        const eject =           0x0100;
        const auto_life =       0x0200;
        const curse =           0x0400;
        const defend =          0x0800;
        const guard =           0x1000;
        const sentinel =        0x2000;
        const doom =            0x4000;
    }
);

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[repr(C)]
pub struct Stats {
    pub strength: u8,
    pub defense: u8,
    pub magic: u8,
    pub magic_defense: u8,
    pub agility: u8,
    pub luck: u8,
    pub evasion: u8,
    pub accuracy: u8,
}

#[derive(Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[repr(C)]
pub struct Status {
    pub death: u8,
    pub zombie: u8,
    pub petrify: u8,
    pub poison: u8,
    pub power_break: u8,
    pub magic_break: u8,
    pub armor_break: u8,
    pub mental_break: u8,
    pub confuse: u8,
    pub berserk: u8,
    pub provoke: u8,
    pub threaten: u8,
    pub sleep: u8,
    pub silence: u8,
    pub darkness: u8,
    pub shell: u8,
    pub protect: u8,
    pub reflect: u8,
    pub nul_water: u8,
    pub nul_fire: u8,
    pub nul_thunder: u8,
    pub nul_ice: u8,
    pub regen: u8,
    pub haste: u8,
    pub slow: u8,
}

#[repr(C)]
pub struct BattleActor {}
